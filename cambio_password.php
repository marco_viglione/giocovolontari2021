<?php
require_once("Auth/Auth.php");
require_once("Database/GestioneDatiGenerici.php");
require_once("Templates/Etichette.php");
require_once("Templates/SezioniPagina.php");

$auth = PHPAuth\Auth::defaultAuth();
$dati = new GestioneDatiGenerici();

if(!$auth->isLogged()) {
	header('Location: ./accesso_volontari.php');
	die();
} else if ($_SERVER['REQUEST_METHOD'] === "POST")
{
	$password_corrente = $_POST['password-corrente'];
	$nuova_password = $_POST['password-nuova'];
	$ripeti_nuova_password = $_POST['password-nuova-ripeti'];

	if (isset($password_corrente) && isset($nuova_password) && isset($ripeti_nuova_password)) {
		$id_giocatore = $auth->getCurrentUser()['id'];
		$risultato = $auth->changePassword($id_giocatore, $password_corrente, $nuova_password, $ripeti_nuova_password);

		$messaggio = $risultato['message'];
		$tipo_messaggio = ($risultato['error'] ? TipiMessaggio::Errore : TipiMessaggio::Successo);
	}
}
?>
<html>
<head>
	<title>Gioco dei volontari - Cambia Password</title>
	<?php SezioniPagina::inserisciCssJavascript(); ?>
</head>
<body class="pb-4">
	<?php
		if (isset($messaggio) && isset($tipo_messaggio)) {
			Etichette::inserisciMessaggio($messaggio, $tipo_messaggio);
		}
	?>
	<div class="container">
		<div class="text-center py-4">
			<h1>Cambia password</h1>
		</div>
		<form id="gv-cambia-password" class="gv-max-width p-3 mx-auto" method="post">
			<div class="form-group">
				<input name="password-corrente" class="form-control" type="password" id="password-corrente" placeholder="Password attuale" required>
			</div>
			<div class="form-group">
				<input name="password-nuova" class="form-control" type="password" id="password-nuova" placeholder="Nuova password" required>
			</div>
			<div class="form-group">
				<input name="password-nuova-ripeti" class="form-control" type="password" id="password-nuova-ripeti" placeholder="Ripeti nuova password" required>
			</div>
			<div class="text-center pt-3">
				<button type="submit" name="btn-cambia-password" class="btn btn-primary">Cambia password</button>
			</div>
		</form>
	</div>
	<?php SezioniPagina::inserisciFooter($auth); ?>
</body>
</html>