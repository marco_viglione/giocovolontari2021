<?php
require_once("Auth/Auth.php");
require_once("Database/GestioneDatiGenerici.php");
require_once("Database/GestioneDatiGioco.php");
require_once("Common/Funzioni.php");
require_once("Templates/Etichette.php");
require_once("Templates/SezioniPagina.php");

$auth = PHPAuth\Auth::defaultAuth();

if(!$auth->isLogged()) {
	header('Location: ./accesso_volontari.php');
	die();
} else {
	$dati_generici = new GestioneDatiGenerici();
	$dati_gioco = new GestioneDatiGioco();
	$funzioni = new Funzioni();

	$punti = 0;
	$punti_in_sospeso = 0;

	$id_giocatore = $auth->getCurrentUser()['id'];
	
    $id_sessione = 0;
	$sessione_odierna = $dati_generici->getSessioneOdierna();
	if ($sessione_odierna) {
		$id_sessione = $sessione_odierna['id_sessione'];
		$punti = $dati_gioco->getPuntiGiocatore($id_giocatore, $id_sessione);
		$punti_in_sospeso = $dati_gioco->getPuntiInSospeso($id_giocatore, $id_sessione);
	}
}
?>
<html>
<head>
	<title>Gioco dei volontari - Punteggio</title>
	<?php SezioniPagina::inserisciCssJavascript(); ?>
</head>
<body class="pb-5">
	<?php
		if (isset($messaggio) && isset($tipo_messaggio)) {
			Etichette::inserisciMessaggio($messaggio, $tipo_messaggio);
		}

		if ($punti !== null) {
			$esiti_da_visualizzare = $dati_gioco->getEsitiDaVisualizzare($id_giocatore, $id_sessione);

			foreach($esiti_da_visualizzare as $esito_da_visualizzare) {
				$dati_giratore = $auth->getUser($esito_da_visualizzare['fk_giratore']);
				$nome_giratore = "";
				if ($dati_giratore['soprannome']) {
					$nome_giratore = $dati_giratore['soprannome'];
				} else {
					$nome_giratore = $dati_giratore['nome'];
				}

				$punti_esito = $funzioni->calcolaRisultato($esito_da_visualizzare['punti_scommessi'], $esito_da_visualizzare['esito']);

				if ($punti_esito > 0) {
					$messaggio = $nome_giratore . " ti ha fatto vincere " . $punti_esito . " punti!";
					$tipo_messaggio = TipiMessaggio::Successo;
				} else {
					$messaggio = $nome_giratore . " ti ha fatto perdere " . abs($punti_esito) . " punti!";
					$tipo_messaggio = TipiMessaggio::Errore;
				}

				Etichette::inserisciMessaggio($messaggio, $tipo_messaggio);

				$dati_gioco->aggiornaStatoEsito($esito_da_visualizzare['id_proposta'], $id_giocatore, StatoEsito::Visualizzato);
			}
		}
	?>
	<div class="container pb-4">
		<?php
			$sessione_iniziata = false;
			$sessione_terminata = false;

			if (!$sessione_odierna) {
				$titolo = 'Oggi non si gioca!';
				$sottotitolo = 'Torna un altro giorno';
			} else {
				if (time() < strtotime($sessione_odierna['ora_inizio'])) {
					$titolo = 'Il gioco non è ancora iniziato';
					$sottotitolo = 'Torna più tardi';
				} else {
					if ($punti === null) {
						$titolo = 'Oggi non sei iscritto';
						$sottotitolo = 'Vuoi giocare? Chiedi in segreteria!';
					} else {
						$sessione_iniziata = true;
						if (time() >= strtotime($sessione_odierna['ora_fine'])) {
							$titolo = 'Il gioco per oggi è finito';
							$sottotitolo = 'Grazie per aver giocato!';
							$sessione_terminata = true;
						} else {
                            $parametri = $dati_generici->getListaParametri();
							$titolo = $parametri['nome_punti'];
						}
					}
				}
			}
		?>
		<div class="text-center pt-4">
			<h1><?php echo $titolo ?></h1>
			<?php
				if(isset($sottotitolo)) {
			?>
			<p class="lead">
				<?php echo $sottotitolo ?><br/>
			</p>
			<?php
				}
			?>
		</div>
        <?php
			if ($sessione_iniziata) {
        ?>
        <div class="tab-content">
            <div id="gv-points" class="rounded-circle">
                <div id="gv-points-inner" class="display-3 font-weight-bold text-center bg-light rounded-circle position-absolute">
                    <?php echo $punti - $punti_in_sospeso ?>
                </div>
            </div>
			<?php if ($punti_in_sospeso > 0) { ?>
				<div class="text-center">
					<small class="form-text text-muted">Hai <?php echo $punti_in_sospeso; ?> punti giocati in attesa di risultato</small>
				</div>
			<?php } ?>
        </div>
		<?php
				if (!$sessione_terminata) {
		?>
        <div class="text-center pt-4">
			<div><a class="btn btn-primary gv-btn-width" href="./proposta.php" role="button">Gioca</a></div>
			<div class="pt-4"><a class="btn btn-primary gv-btn-width" href="./punteggio.php" role="button">Aggiorna punti</a></div>
		</div>
        <?php
				}
            }
        ?>
	</div>
	<?php SezioniPagina::inserisciFooter($auth); ?>
</body>
</html>