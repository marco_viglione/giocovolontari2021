<?php

class Log {

  private $log_path = './Log/error_log';

  /**
  * Centralizza il logging degli errori
  *
  * Parametri di input
  * @param string $errore: il messaggio da loggare
  *
  */
  public function loggaErrore($errore) {
    error_log($errore . PHP_EOL, 3, $this->log_path);
  }
}
?>