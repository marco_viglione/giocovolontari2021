<?php
require_once(dirname(__DIR__) . "/Common/Enum.php");
require_once(dirname(__DIR__) . "/Database/GestioneDatiGenerici.php");

class DatiRisultato {
  public $titolo;
  public $testo;
  public $icona;
}

class FasciaOraria {
  public $ora_inizio;
  public $ora_fine;
}

class Funzioni {

  private $dati_generici;

  public function __construct() { 
    $this->dati_generici = new GestioneDatiGenerici();
  }

  /**
   * Costruisce il nome del giocatore da mostrare a schermo.
   * L'ho dovuto fare talmente tante volte che ho deciso di centralizzare la funzione.
   *
   * Parametri di input
   * @param var $dati_giocatore: array di dati corrispondente a una riga della tabella phpauth_users
   *
   * Risultato
   * @return nome del giocatore
   */
  public function costruisciNomeGiocatore($dati_giocatore) {
      $nome_giocatore = $dati_giocatore['nome'] . ' ' . $dati_giocatore['cognome'];
      if (!empty($dati_giocatore['soprannome'])) {
        $nome_giocatore .= ' (' . $dati_giocatore['soprannome'] . ')';
      }

      return $nome_giocatore;
  }

  /**
   * Genera una password per l'utente a partire da nome e cognome.
   * Una roba del genere non andrebbe fatta mai nel modo più assolutissimamente assoluto, ma ehi, siamo solo un gioco dei volontari, possiamo permettercelo.
   *
   * Parametri di input
   * @param string $nome_utente: nome dell'utente
   * @param string $cognome_utente: cognome dell'utente
   *
   * Risultato
   * @return string|null
   */
  public function generaPassword($nome_utente, $cognome_utente){
    if (isset($nome_utente) && isset($cognome_utente)) {
      //Rimuovo caratteri speciali e spazi
      $nome = preg_replace('/[^A-Za-z]/', '', $nome_utente);
      $cognome = preg_replace('/[^A-Za-z]/', '', $cognome_utente);

      return strtolower($nome . $cognome);
    }
    else {
      return null;
    }
  }

  /**
   * Calcola i punti vinti o persi in base ai punti scommessi e all'esito, arrotondati per eccesso
   * 
   * Parametri di input
   * @param int $punti_scommessi: il numero di punti scommessi
   * @param int $esito: l'identificativo dell'esito (vedi Enum:RisultatoGioco)
   * 
   * Risultato
   * @return int
   */
  public function calcolaRisultato($punti_scommessi, $esito) {
    $result = $punti_scommessi;

    if (is_numeric($result)) {
      switch($esito) {
        case RisultatoGioco::Raddoppia :
          $result = $result * 2;
          break;
        case RisultatoGioco::Vinci :
          break;
        case RisultatoGioco::VinciPoco :
          $result = round($result/2);
          break;
        case RisultatoGioco::PerdiPoco :
          $result = round($result/2) * -1;
          break;
        case RisultatoGioco::Perdi :
          $result = $result * -1;
          break;
        default :
        break;
      }
    }

    return $result;
  }

  /**
   * Restituisce i dati relativi all'esito specificato
   * 
   * Parametri di input
   * @param int $esito: l'identificativo dell'esito (vedi Enum:RisultatoGioco)
   * 
   * Risultato
   * @return DatiRisultato
   * 
   * Campi attesi dell'oggetto di output
   * $titolo: il titolo da visualizzare nella finestra del risultato
   * $testo: il testo del risultato
   * $icona: la classe di Font Awesome 4 corrispondente al risultato
   */
  public function getDatiRisultato($esito) {
    $risultato = new DatiRisultato();

    switch($esito) {
      case RisultatoGioco::Raddoppia :
        $risultato->titolo = 'HAI SCUBATO!!';
        $risultato->testo = 'Si vede che di portafortuna te ne intendi.<br/>Vinci il doppio dei punti!';
        $risultato->icona = 'fa-diamond';
        break;
      case RisultatoGioco::Vinci :
        $risultato->titolo = 'CHE CUBO!';
        $risultato->testo = 'Con un certo savoir faire sai con chi devi giochère.<br/>Vinci i punti che hai giocato!';
        $risultato->icona = 'fa-cubes';
        break;
      case RisultatoGioco::VinciPoco :
        $risultato->titolo = 'UNO SCUBELLO!';
        $risultato->testo = 'Tra di voi non c\'è molta sintonia, magari dovete conoscervi meglio.<br/>Vinci la metà dei punti giocati!';
        $risultato->icona = 'fa-cube';
        break;
      case RisultatoGioco::PerdiPoco :
        $risultato->titolo = 'MA CHE SFOGA!';
        $risultato->testo = 'Mi sa che dovete lavorare sulla fortuna perché tra di voi ce n\'è molta poca.<br/>Perdi la metà dei punti giocati!';
        $risultato->icona = 'fa-thumbs-down';
        break;
      case RisultatoGioco::Perdi :
        $risultato->titolo = 'IELLAZONATO!';
        $risultato->testo = 'Non ci siamo, tra di voi aleggia la iella.<br/>Perdi tutti i punti giocati!';
        $risultato->icona = 'fa-user-times';
        break;
      default :
        break;
    }

    return $risultato;
  }

  /**
   * Restituisce la fascia oraria di gioco in cui ci si trova attualmente
   * 
   * Risultato
   * @return FasciaOraria
   * 
   * Campi attesi dell'oggetto di output
   * $ora_inizio: l'ora di inizio della fascia oraria
   * $ora_fine: l'ora di fine della fascia oraria
   */
  public function getFasciaOraria() {
    $risultato = new FasciaOraria();

    $parametri = $this->dati_generici->getListaParametri();
    $sessione_odierna = $this->dati_generici->getSessioneOdierna();

    if (isset($sessione_odierna)) {
      $risultato->ora_inizio = $sessione_odierna['ora_inizio'];
      $risultato->ora_fine = date("H:i:s", strtotime($risultato->ora_inizio) + 60*60*$parametri['ore_attesa_tentativi']);
  
      while(time() > strtotime($risultato->ora_fine)) {
        $risultato->ora_inizio = date("H:i:s", strtotime($risultato->ora_inizio) + 60*60*$parametri['ore_attesa_tentativi']);
        $risultato->ora_fine = date("H:i:s", strtotime($risultato->ora_fine) + 60*60*$parametri['ore_attesa_tentativi']);

        if ($risultato->ora_fine == '00:00:00') {
          $risultato->ora_fine = date("H:i:s", strtotime($risultato->ora_fine) - 1);
        }
      }

      return $risultato;
    } else {
      return null;
    }
  }
}
?>