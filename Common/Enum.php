<?php
    abstract class RisultatoGioco
    {
        const PerdiPoco = 1;
        const VinciPoco = 2;
        const Perdi = 3;
        const Vinci = 4;
        const Raddoppia = 5;
    }

    abstract class StatoProposta
    {
        const Pendente = 0;
        const Accettata = 1;
    }

    abstract class StatoEsito
    {
        const Assente = 0;
        const Calcolato = 1;
        const Assegnato = 2;
        const Visualizzato = 3;
    }

    abstract class TipoClassifica
    {
        const Punti = 0;
        const Fortuna = 1;
    }
?>