<?php
require("./vendor/autoload.php");

require_once("Auth/Auth.php");
require_once("Database/GestioneDatiGenerici.php");
require_once("Funzioni.php");
require_once("Log.php");

use PHPMailer\PHPMailer\PHPMailer;

abstract class SistemaInvioMail
{
    const Base = 0;
    const PHPMailer = 1;
}

class DatiMail {
  public $mail_destinatario;
  public $mail_mittente;
  public $nome_mittente;
  public $oggetto;
  public $testo;
}

class Mail {

  private $auth;
  private $dati;
  private $funzioni;
  private $log;
  private $sistema_invio_mail;

  public function __construct() {
      $this->auth = PHPAuth\Auth::defaultAuth();
      $this->dati = new GestioneDatiGenerici();
      $this->funzioni = new Funzioni();
      $this->log = new Log();
      $this->sistema_invio_mail = SistemaInvioMail::Base;
  }

  /**
  * Invia a un giocatore una mail con le sue credenziali iniziali e memorizza l'esito dell'invio sul database.
  *
  * Parametri di input
  * @param int $id_giocatore: identificativo del giocatore
  *
  * Risultato
  * @return esito dell'invio (true/false)
  */
  public function inviaMailCredenziali($id_giocatore) {
    $dati_giocatore = $this->auth->getUser($id_giocatore);
    $parametri = $this->dati->getListaParametri();

    $tempo_attesa_tentativi = '';
    if ($parametri['ore_attesa_tentativi'] == 1) {
      $tempo_attesa_tentativi = '1 ora';
    } else {
      $tempo_attesa_tentativi = $parametri['ore_attesa_tentativi'] . ' ore';
    }

    $testo = file_get_contents('./Templates/Mail/InvioCredenziali.html');
    $testo = str_replace('{{USERNAME}}', $dati_giocatore['email'], $testo);
    $testo = str_replace('{{DEFAULT_PWD}}', $this->funzioni->generaPassword($dati_giocatore['nome'], $dati_giocatore['cognome']), $testo);
    $testo = str_replace('{{STARTING_POINTS}}', $parametri['punti_iniziali'] . ' ' . $parametri['nome_punti'], $testo);
    $testo = str_replace('{{ATTEMPTS_WAIT}}', $tempo_attesa_tentativi, $testo);

    $dati_mail = new DatiMail();
    $dati_mail->mail_destinatario = $dati_giocatore['email'];
    $dati_mail->oggetto = 'Gioco dei volontari - Credenziali di accesso';
    $dati_mail->testo = $testo;

    if ($this->inviaMail($dati_mail)) {
        $this->dati->impostaCredenzialiInviate($id_giocatore);
        return true;
    } else {
        return false;
    }
  }

  /**
  * Invia a un giocatore una mail per avvisarlo dell'inizio del gioco.
  *
  * Parametri di input
  * @param int $id_giocatore: identificativo del giocatore
  * @param int $id_sessione: identificativo della sessione
  *
  * Risultato
  * @return esito dell'invio (true/false)
  */
  public function inviaMailInizioGioco($id_giocatore, $id_sessione) {
    $dati_giocatore = $this->auth->getUser($id_giocatore);
    $dati_sessione = $this->dati->getDatiSessione($id_sessione);

    $data = new DateTime($dati_sessione['data_sessione']);
    $testo = file_get_contents('./MailTemplates/AvvisoInizioGioco.html');
    $testo = str_replace('{{END_TIME}}', date('H:i', strtotime($dati_sessione['ora_fine'])), $testo);

    $dati_mail = new DatiMail();
    $dati_mail->mail_destinatario = $dati_giocatore['email'];
    $dati_mail->oggetto = 'Gioco dei volontari - Inizio gioco (' . $data->format('d/m/Y') . ')';
    $dati_mail->testo = $testo;

    return $this->inviaMail($dati_mail);    
  }

  /**
  * Richiama il metodo di invio mail corrispondente al sistema di invio specificato nel costruttore.
  *
  * Parametri di input
  * @param var $dati_mail: oggetto contenenti i dati della mail da inviare
  *
  * Risultato
  * @return esito dell'invio (true/false)
  */
  public function inviaMail($dati_mail) {
    $dati_mail->mail_mittente = 'giocodeivolontari@giocaosta.it';
    $dati_mail->nome_mittente = 'Gioco dei volontari';

    switch ($this->sistema_invio_mail) {
      case SistemaInvioMail::Base:
        return $this->inviaMailBase($dati_mail);
        break;
      case SistemaInvioMail::PHPMailer:
        return $this->inviaMailPHPMailer($dati_mail);
        break;
      default:
        return false;
        break;
    }
  }

  /**
  * Invia una mail utilizzando la funzione di default di PHP.
  *
  * Parametri di input
  * @param var $dati_mail: oggetto contenenti i dati della mail da inviare
  *
  * Risultato
  * @return esito dell'invio (true/false)
  */
  public function inviaMailBase($dati_mail) {
    $headers = array(
        "MIME-Version" => "1.0",
        "Content-type" => "text/html; charset=utf-8",
        "From" => $dati_mail->nome_mittente . "<" . $dati_mail->mail_mittente . ">",
        //"Reply-To" => $dati_mail->mail_mittente,
        //"Return-Path" => $dati_mail->mail_mittente,
        //"X-MSMail-Priority" => "High",
        "X-Mailer" => "PHP/" . phpversion()
    );

    $testo_mail = wordwrap($dati_mail->testo, 70);

    $esito = mail($dati_mail->mail_destinatario, $dati_mail->oggetto, $testo_mail, $headers);

    if(!$esito){
      $this->log->loggaErrore(error_get_last()['message']);
    }

    return $esito;
  }

  /**
  * Invia una mail utilizzando PHPMailer.
  *
  * Parametri di input
  * @param var $dati_mail: oggetto contenenti i dati della mail da inviare
  *
  * Risultato
  * @return esito dell'invio (true/false)
  */
  public function inviaMailPHPMailer($dati_mail) {
    $parametri = $this->auth->config;
    $mail = new PHPMailer();

    $mail->isSMTP();

    //$mail->SMTPDebug = 2;
    //$mail->Debugoutput = 'html';

    $mail->Host = $parametri->smtp_host;
    $mail->Port = $parametri->smtp_port;

    if (!is_null($parametri->smtp_security)) {
      $mail->SMTPSecure = $parametri->smtp_security;
    }

    $mail->SMTPAuth = $parametri->smtp_auth;

    if (!is_null($parametri->smtp_auth)) {
      $mail->Username = $parametri->smtp_username;
      $mail->Password = $parametri->smtp_password;
    }

    $mail->setFrom($dati_mail->mail_mittente, $dati_mail->nome_mittente);
    $mail->addAddress($dati_mail->mail_destinatario);
    $mail->Subject = $dati_mail->oggetto;
    $mail->isHTML(true);
    $mail->Body = $dati_mail->testo;

    $esito = $mail->send();

    if(!$esito){
      $this->log->loggaErrore($mail->ErrorInfo);
    }

    return $esito;
  }
}
?>