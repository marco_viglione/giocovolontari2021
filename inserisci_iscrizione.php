<?php
require_once("Auth/Auth.php");
require_once("Database/GestioneDatiGenerici.php");
require_once("Database/GestioneDatiGioco.php");
require_once("Common/Funzioni.php");
require_once("Templates/Etichette.php");
require_once("Templates/SezioniPagina.php");

$auth = PHPAuth\Auth::defaultAuth();
$dati_generici = new GestioneDatiGenerici();
$dati_gioco = new GestioneDatiGioco();
$funzioni = new Funzioni();

$dati_giocatore = null;
$sessioni_giocatore = [];
$titolo_pagina = 'Inserisci iscrizione';
$etichetta_bottone = 'Inserisci';

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: ./accesso_volontari.php');
	die();
} else if ($_SERVER['REQUEST_METHOD'] === "POST")
{
	$email = trim($_POST['mail']);
	$nome = trim($_POST['nome']);
	$cognome = trim($_POST['cognome']);
	$soprannome = trim($_POST['soprannome']);

	if (isset($email) && strlen(trim($nome)) > 0 && strlen(trim($cognome)) > 0) {
		$parametri = $dati_generici->getListaParametri();

		if ($_POST['id_giocatore'] > 0) {
			$id_mail = $auth->getUID($email);

			if ($id_mail == $_POST['id_giocatore'] || $id_mail == 0) {
				$dati_aggiornati = [];
				$dati_aggiornati['email'] = $email;
				$dati_aggiornati['nome'] = $nome;
				$dati_aggiornati['cognome'] = $cognome;
				$dati_aggiornati['soprannome'] = $soprannome;

				$sessioni_giocatore = $dati_generici->getIscrizioniGiocatore($_POST['id_giocatore']);
				$sessioni_aggiornate = [];
				foreach ($_POST as $key => $value) {
					if (substr($key, 0, 12) === 'gv-sessione-') {
						array_push($sessioni_aggiornate, substr($key, 12, strlen($key)));
					}
				}
	
				$auth->updateUser($_POST['id_giocatore'], $dati_aggiornati);

				foreach (array_diff($sessioni_giocatore, $sessioni_aggiornate) as $sessione_eliminata) {
					$dati_generici->eliminaIscrizioneGiocatore($_POST['id_giocatore'], $sessione_eliminata);
				}

				foreach (array_diff($sessioni_aggiornate, $sessioni_giocatore) as $sessione_aggiunta) {
					$dati_generici->inserisciIscrizioneGiocatore($_POST['id_giocatore'], $sessione_aggiunta);
					if ($dati_gioco->getPuntiGiocatore($_POST['id_giocatore'], $sessione_aggiunta) == null) {
						$dati_gioco->assegnaPuntiGiocatore($_POST['id_giocatore'], $sessione_aggiunta, $parametri['punti_iniziali']);
					}
				}

				$messaggio = 'Giocatore modificato con successo!';
				$tipo_messaggio = TipiMessaggio::Successo;
			} else {
				$messaggio = 'Operazione non effettuata. L\'indirizzo email specificato è già usato da un altro giocatore.';
				$tipo_messaggio = TipiMessaggio::Avviso;
			}
		} else {
			$password = $funzioni->generaPassword($nome, $cognome);
			if (isset($password)) {
				$params = array("nome" => $nome, "cognome" => $cognome, "soprannome" => $soprannome, "role" => 1);
				$registrationResult = $auth->register($email, $password, $password, $params);
				if($registrationResult['error']  == false)
				{
					$id_giocatore_inserito = $auth->getUID($email);
					foreach ($_POST as $key => $value) {
						if (substr($key, 0, 12) === 'gv-sessione-') {
							$dati_generici->inserisciIscrizioneGiocatore($id_giocatore_inserito, substr($key, 12, strlen($key)));
							$dati_gioco->assegnaPuntiGiocatore($id_giocatore_inserito, substr($key, 12, strlen($key)), $parametri['punti_iniziali']);
						}
					}
					$messaggio = 'Giocatore inserito con successo!';
					$tipo_messaggio = TipiMessaggio::Successo;
				}
				else
				{
					$messaggio = $registrationResult['message'];
					$tipo_messaggio = TipiMessaggio::Errore;
				}
			} else {
				$messaggio = 'Operazione non effettuata. Dati incompleti.<br/>Per un nuovo giocatore è necessario inserire almeno nome e cognome.';
				$tipo_messaggio = TipiMessaggio::Avviso;
			}
		}
	} else {
		$messaggio = 'Operazione non effettuata. Dati obbligatori mancanti.';
		$tipo_messaggio = TipiMessaggio::Avviso;
	}
}

if (isset($_GET['id']))
{
	$id_giocatore = $_GET['id'];

	if (isset($id_giocatore)) {
		$dati_giocatore = $auth->getUser($id_giocatore);
		$sessioni_giocatore = $dati_generici->getIscrizioniGiocatore($id_giocatore);

		$titolo_pagina = 'Modifica iscrizione';
		$etichetta_bottone = 'Modifica';
	}
}
?>
<html>
<head>
	<title>Gioco dei volontari - Inserisci iscrizione</title>
	<?php SezioniPagina::inserisciCssJavascript(); ?>
</head>
<body class="pb-4">
	<?php
		if (isset($messaggio) && isset($tipo_messaggio)) {
			Etichette::inserisciMessaggio($messaggio, $tipo_messaggio);
		}
	?>
	<div class="container">
	<?php
		$sessioni = $dati_generici->getListaSessioni(date("Y"), true);

		if (count($sessioni) > 0) {
	?>
		<div class="text-center py-4">
			<h1><?php echo $titolo_pagina; ?></h1>
		</div>
		<form id="gv-inserisci-giocatore" class="gv-max-width p-3 mx-auto" method="post">
			<input id="id_giocatore" name="id_giocatore" type="hidden" value="<?php if (isset($id_giocatore)) { echo $id_giocatore; } else { echo 0; } ?>">
			<div class="form-group">
				<input name="mail" class="form-control" type="email" id="mail" placeholder="Indirizzo email" value="<?php if(isset($dati_giocatore) && isset($dati_giocatore['email'])) { echo $dati_giocatore['email']; } ?>" required>
			</div>
			<div class="form-group">
				<input name="nome" class="form-control" type="text" id="nome" placeholder="Nome" value="<?php if(isset($dati_giocatore) && isset($dati_giocatore['nome'])) { echo $dati_giocatore['nome']; } ?>">
			</div>
			<div class="form-group">
				<input name="cognome" class="form-control" type="text" id="cognome" placeholder="Cognome" value="<?php if(isset($dati_giocatore) && isset($dati_giocatore['cognome'])) { echo $dati_giocatore['cognome']; } ?>">
			</div>
			<div class="form-group">
				<input name="soprannome" class="form-control" type="text" id="soprannome" placeholder="Soprannome" value="<?php if(isset($dati_giocatore) && isset($dati_giocatore['soprannome'])) { echo $dati_giocatore['soprannome']; } ?>">
			</div>
			<div class="text-center">
				<p>Iscritto per i giorni:</p>
				<div class="form-group">
					<?php
					foreach ($sessioni as $sessione) {
						$id_riga_sessione = "gv-sessione-" . $sessione['id_sessione'];
						$data = new DateTime($sessione['data_sessione']);
						$str_data = $data->format('d/m/Y');
						?>
						<div class="d-sm-inline-block px-2">
							<input name="<?php echo $id_riga_sessione ?>" type="checkbox" id="<?php echo $id_riga_sessione ?>" <?php if(in_array($sessione['id_sessione'], $sessioni_giocatore)) { echo 'checked'; } ?>>
							<label class="checkbox-inline" for="<?php echo $id_riga_sessione ?>"><?php echo $str_data ?></label>
						</div>
						<?php
					}
					?>
				</div>
			</div>
			<div class="text-center pt-3">
				<button type="submit" name="btn-inserisci" class="btn btn-primary gv-btn-width"><?php echo $etichetta_bottone; ?></button>
				<div class="pt-4"><a class="btn btn-primary gv-btn-width" href="./gestione_giocatori.php" role="button">Torna all'elenco</a></div>
			</div>
		</form>
	<?php
		} else {
	?>
		<div class="text-center pt-4 pb-2">
			<h1>Operazione non eseguibile</h1>
			<p class="lead">Non esistono sessioni di gioco disponibili.</p>
		</div>
	<?php
		}
	?>
	</div>
	<?php SezioniPagina::inserisciFooter($auth); ?>
</body>
</html>