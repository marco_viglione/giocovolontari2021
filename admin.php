<?php
require_once("Auth/Auth.php");
require_once("Templates/Etichette.php");
require_once("Templates/SezioniPagina.php");

$auth = PHPAuth\Auth::defaultAuth();

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: ./accesso_volontari.php');
	die();
}
?>
<html>
<head>
	<title>Gioco dei volontari - Pannello di amministrazione</title>
	<?php SezioniPagina::inserisciCssJavascript(); ?>
</head>
<body class="pb-5">
	<div class="container pb-3">
		<div class="text-center pt-4">
			<h1>Pannello di amministrazione</h1>
			<p class="lead">
				Scegli un'operazione da effettuare
			</p>
		</div>
		<div class="text-center">
			<div class="pt-4"><a class="btn btn-primary gv-btn-width" href="./gestione_giocatori.php" role="button">Gestione giocatori</a></div>
			<div class="pt-4"><a class="btn btn-primary gv-btn-width" href="./invio_credenziali.php" role="button">Invia credenziali</a></div>
			<div class="pt-4"><a class="btn btn-primary gv-btn-width" href="./assegna_punti.php" role="button">Assegna punti</a></div>
			<div class="pt-4"><a class="btn btn-primary gv-btn-width" href="./classifica.php" role="button">Classifica</a></div>
		</div>
	</div>
	<?php SezioniPagina::inserisciFooter($auth); ?>
</body>
</html>