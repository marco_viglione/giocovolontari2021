<?php
require_once("Auth/Auth.php");
require_once("Database/GestioneDatiGenerici.php");
require_once("Database/GestioneDatiGioco.php");
require_once("Templates/Etichette.php");
require_once("Templates/SezioniPagina.php");

$auth = PHPAuth\Auth::defaultAuth();

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: ./accesso_volontari.php');
	die();
} else {
    $dati_generici = new GestioneDatiGenerici();
    $dati_gioco = new GestioneDatiGioco();

    $sessione_odierna = $dati_generici->getSessioneOdierna();

    if ($_SERVER['REQUEST_METHOD'] === "POST") { 
        if (isset($_POST['gv-id-volontario']) && isset($_POST['gv-punti-assegnati']) && isset($_POST['btn-invia'])) {
            $success = $dati_gioco->assegnaPuntiGiocatore($_POST['gv-id-volontario'], $sessione_odierna['id_sessione'], $_POST['gv-punti-assegnati'], true);

            if ($success) {
                $messaggio = "Punti assegnati correttamente!";
                $tipo_messaggio = TipiMessaggio::Successo;
            } else {
                $messaggio = "Si è verificato un errore. Riprova.";
                $tipo_messaggio = TipiMessaggio::Errore;
            }
        }
    }
}
?>
<html>
<head>
	<title>Gioco dei volontari - Assegna punti</title>
	<?php SezioniPagina::inserisciCssJavascript(); ?>
</head>
<body class="pb-5">
	<?php
		if (isset($messaggio) && isset($tipo_messaggio)) {
			Etichette::inserisciMessaggio($messaggio, $tipo_messaggio);
		}
	?>
	<div class="container pb-4">
		<?php
            if ($sessione_odierna) {
		?>
		<div class="text-center py-4">
			<h1>Assegna punti</h1>
		</div>
        <form class="p-3 mx-auto gv-max-width" method="post">
            <div class="form-group row text-center">
                <label for="gv-volontario" class="col-sm-5 col-form-label">Volontario</label>
                <div class="col-sm-7 pb-1">
                    <input id="gv-volontario" name="gv-volontario" class="form-control" placeholder="Cerca per nome o soprannome" required>
                    <small class="form-text text-muted">Punti posseduti: <span id="gv-punti">0</span></small>
                </div>
                <input id="gv-id-volontario" name="gv-id-volontario" type="hidden">
            </div>
            <div class="form-group row text-center">
				<label for="gv-punti-assegnati" class="col-sm-5 col-form-label">Punti da assegnare</label>
                <div class="col-sm-7">
                    <input id="gv-punti-assegnati" name="gv-punti-assegnati" class="gv-input-width-sm no-focus" required readonly>
                </div>
			</div>
            <div class="text-center pt-3">
				<button type="submit" name="btn-invia" class="btn btn-primary gv-btn-width">Assegna</button>
			</div>
        </form>
	</div>
    <?php
		} else {
	?>
		<div class="text-center pt-4 pb-2">
			<h1>Operazione non eseguibile</h1>
			<p class="lead">Non è impostata una sessione di gioco per oggi.</p>
		</div>
	<?php
		}
	?>
	<?php SezioniPagina::inserisciFooter($auth); ?>
</body>
<script type="text/javascript">
    $( function() {
        <?php if ($sessione_odierna) { ?>
        // Spinner
        var spinner = $("#gv-punti-assegnati").spinner({
            min: -1000,
            max: 1000,
            start: 1
        });

        spinner.spinner("value", 0);

        // Autocomplete
        var autocomplete = $("#gv-volontario").autocomplete({
            source: "Ajax/RicercaVolontari.php",
            minLength: 3,
            select: function(event, ui) {
                $("#gv-volontario").val(ui.item.volontario);
                $("#gv-id-volontario").val(ui.item.id);

                $.ajax({ url: 'Ajax/ControllaPunti.php',
                    data: {
                        fk_volontario: ui.item.id,
                        fk_sessione: <?php echo $sessione_odierna['id_sessione']; ?>
                    },
                    type: 'post',
                    success: function(response) {
                        ajax_result = $.parseJSON(response);
                        $('#gv-punti').html(ajax_result.punti);
                    }
                });
        
                return false;
            }
        });

        autocomplete.autocomplete("widget").addClass("list-group");

        autocomplete.autocomplete("instance")._renderItem = function(ul, item) {
            return $("<li class=\"list-group-item\"></li>").data("item.autocomplete", item).append(item.volontario).appendTo(ul);
        };

        $("#gv-volontario").keyup(function() {
            if ($("#gv-volontario").val() === '') {
                $('#gv-punti').html(0);
            }
        });
        <?php } ?>
    });
</script>
</html>