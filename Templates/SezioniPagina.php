<?php
class SezioniPagina {

  /**
   *Inserisce la parte dell'header contenente i metadati, i js e i css
   */
  public static function inserisciCssJavascript()
  {
    ?>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- JQuery -->
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

    <!-- Bootstrap Toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <!-- JQueryUI -->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <!-- FontAwesome -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    
    <!-- CSS -->
    <link rel="stylesheet" href="css/main.css" >
    
    <?php
  }

  /**
   * Inserisce il footer con il link per effettuare login\logout
   */
  public static function inserisciFooter($auth)
  {
    ?>
    <footer class="fixed-bottom bg-light py-1">
      <div class="container">
        <div class="row">
          <?php
          if ($auth->isLogged()) {
          ?>
            <div class="col-auto">
              <?php echo "Ciao <b>".$auth->getCurrentUser()['nome']."!</b>"; ?>
            </div>
            <div class="col-md">
            <?php
              if ($auth->isAdmin()) {
              ?>
              <a href="./admin.php">Amministrazione</a>
              <?php
              }
            ?>
              <div class="float-right">
                <a class="ml-1 mr-sm-3" href="./cambio_password.php">Cambia password</a>
                <a class="ml-2" href="./logout.php">Logout</a>
              </div>
            </div>
          <?php
          }
          else {
          ?>
            <a href="./accesso_volontari.php">Login</a>
          <?php
          }
          ?>
        </div>
      </div>
    </footer>
    <?php
  }
}
?>