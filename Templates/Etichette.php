<?php
abstract class TipiMessaggio
{
    const Successo = 0;
    const Avviso = 1;
    const Errore = 2;
    const Informazione = 3;
}

class Etichette {

  public static function inserisciMessaggio($messaggio, $tipo_messaggio)
  {
    if (isset($messaggio)) {
      switch ($tipo_messaggio) {
        case TipiMessaggio::Successo :
          $classe = "alert-success";
          break;
        case TipiMessaggio::Avviso :
          $classe = "alert-warning";
          break;
        case TipiMessaggio::Errore :
          $classe = "alert-danger";
          break;
        case TipiMessaggio::Informazione :
          $classe = "alert-info";
          break;
        default :
          $classe = "alert-secondary";
          break;
      }
?>
    <div class="alert <?php echo $classe ?> text-center fade show" role="alert">
      <?php echo $messaggio ?>
    </div>
<?php
    }
  }

}
?>