<?php
require_once("Auth/Auth.php");
require_once("Common/Funzioni.php");
require_once("Common/Mail.php");
require_once("Database/GestioneDatiGenerici.php");
require_once("Templates/SezioniPagina.php");
require_once("Templates/Etichette.php");

$auth = PHPAuth\Auth::defaultAuth();
$dati = new GestioneDatiGenerici();
$funzioni = new Funzioni();
$mail = new Mail();

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: ./accesso_volontari.php');
	die();
} else if ($_SERVER['REQUEST_METHOD'] === "POST")
{
	$operazione_effettuata = true;
	foreach ($_POST as $key => $value) {
		if (substr($key, 0, 19) === 'gv-chk-credenziali-') {
			$esito = $mail->inviaMailCredenziali($value);
			if ($operazione_effettuata && !$esito){
				$operazione_effettuata = false;
			}
		}
	}

	if ($operazione_effettuata){
		$messaggio = 'Credenziali inviate!';
		$tipo_messaggio = TipiMessaggio::Successo;
	} else {
		$messaggio = 'Si sono verificati degli errori. I giocatori ancora preselezionati potrebbero non aver ricevuto le credenziali.';
		$tipo_messaggio = TipiMessaggio::Avviso;
	}
}
?>
<html>
<head>
	<title>Gioco dei volontari - Invia credenziali</title>
	<?php SezioniPagina::inserisciCssJavascript(); ?>
</head>
<body class="pb-5">
	<?php
		if (isset($messaggio) && isset($tipo_messaggio)) {
			Etichette::inserisciMessaggio($messaggio, $tipo_messaggio);
		}
	?>
	<div class="container">
		<div class="text-center pt-4">
			<h1>Invia credenziali</h1>
			<p class="lead">
				Seleziona i volontari a cui inviare le credenziali iniziali
			</p>
		</div>
		<div id="ge-search" class="form-group position-relative text-center ml-auto mr-auto pt-2">
			<label for="ge-searchbox">
				<i class="fa fa-search"></i>
			</label>
			<input id="ge-searchbox" type="text" name="game-search" placeholder="Cerca volontario" spellcheck="false">
		</div>
		<form id="gv-invia-credenziali" class="gv-compact-width pb-3 mx-auto text-center" method="post">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center gv-chk-width"><input type="checkbox" id="multi-select"></th>
						<th>Volontario</th>
					</tr>
				</thead>
				<tbody>
	<?php
		$chk_tabella = 0;
		$chk_selezionati = 0;
		$lista_giocatori = $dati->getListaGiocatori();

		foreach ($lista_giocatori as $giocatore) {
			$id_checkbox = 'gv-chk-credenziali-' . $giocatore['id'];

			$chk_tabella++;
			if(!$giocatore['credenziali_inviate']) {
				$chk_selezionati++;
			}
	?>
				<tr class="gv-form-check">
					<td class="text-center gv-chk-width">
						<input class="form-check-volontario" type="checkbox" value="<?php echo $giocatore['id'] ?>" id="<?php echo $id_checkbox ?>" name="<?php echo $id_checkbox ?>" <?php echo $giocatore['credenziali_inviate'] ? '' : 'checked' ?>>
				  	</td>
				  	<td><label class="form-check-label" for="<?php echo $id_checkbox ?>"><?php echo $funzioni->costruisciNomeGiocatore($giocatore) ?></label></td>
				</tr>
	<?php
		}
	?>
				</tbody>
			</table>
			<div class="text-center pt-3">
				<button type="submit" name="btn-invia-credenziali" class="btn btn-primary">Invia credenziali</button>
			</div>
		</form>
	</div>
	<?php SezioniPagina::inserisciFooter($auth); ?>
</body>
</html>
<script>
	$(function() {
		$("#multi-select").prop('checked', <?php echo $chk_selezionati; ?> == <?php echo $chk_tabella; ?>);
	});

	$("#multi-select").change(function () {
	    $('input:checkbox.form-check-volontario:visible').prop('checked', this.checked);
	});

	$("#ge-searchbox").on("keyup", function(key) {
		var value = $(this).val().toLowerCase();

		if (key.which == 13) {
			$("tr.gv-form-check").each(function(index) {
				$nome_volontario = $(this).find("label").text().toLowerCase();
	
				if ($nome_volontario.indexOf(value) < 0) {
					$(this).hide();
				}
				else {
					$(this).show();
				}
			});

			$("#multi-select").prop('checked', $("input.form-check-volontario:visible:checked").length == $("input.form-check-volontario:visible").length);
		}
		else if (!value) {
			$("tr.gv-form-check").each(function(index) {
				$(this).show();
			});

			if($("input.form-check-volontario:checked").length < $("input.form-check-volontario").length) {
				$("#multi-select").prop('checked', false);
			}
		}
	});
</script>