<?php
    require_once(dirname(__DIR__) . "/Database/GestioneDatiGioco.php");

    $dati_gioco = new GestioneDatiGioco();

    if(isset($_POST['fk_proposta']) && isset($_POST['fk_volontario'])) {
        $risultato = $dati_gioco->assegnaRisultatoGioco($_POST['fk_volontario'], $_POST['fk_proposta']);

        echo json_encode(array("success" => $risultato));
    }
?>