<?php
    require_once(dirname(__DIR__) . "/Auth/Auth.php");
    require_once(dirname(__DIR__) . "/Database/GestioneDatiGioco.php");

    $auth = PHPAuth\Auth::defaultAuth();
    $dati_gioco = new GestioneDatiGioco();

    $id_utente = $auth->getCurrentUser()['id'];

    if(isset($_POST['fk_volontario'])) {
        $risultato = $dati_gioco->countProposteVolontari($id_utente, $_POST['fk_volontario']);

        echo json_encode(array("count" => $risultato));
    }
?>