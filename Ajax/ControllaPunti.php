<?php
    require_once(dirname(__DIR__) . "/Auth/Auth.php");
    require_once(dirname(__DIR__) . "/Database/GestioneDatiGioco.php");

    $auth = PHPAuth\Auth::defaultAuth();
    $dati_gioco = new GestioneDatiGioco();

    $id_utente = $auth->getCurrentUser()['id'];

    if(isset($_POST['fk_volontario']) && isset($_POST['fk_sessione'])) {
        $risultato = $dati_gioco->getPuntiGiocatore($_POST['fk_volontario'], $_POST['fk_sessione']);
        if ($risultato === null) {
            $risultato = 0;
        }

        echo json_encode(array("punti" => $risultato));
    }
?>