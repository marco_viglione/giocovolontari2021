<?php
    require_once(dirname(__DIR__) . "/Auth/Auth.php");
    require_once(dirname(__DIR__) . "/Database/GestioneDatiGenerici.php");

    $auth = PHPAuth\Auth::defaultAuth();

    $dati_generici = new GestioneDatiGenerici();
    $sessione_odierna = $dati_generici->getSessioneOdierna();
    $id_utente = $auth->getCurrentUser()['id'];

    if(isset($_GET['term']) && isset($sessione_odierna) && isset($id_utente)) {
        $risultati = $dati_generici->ricercaGiocatoriSessione($sessione_odierna['id_sessione'], $_GET['term'], array($id_utente));
        echo json_encode($risultati);
    }
?>