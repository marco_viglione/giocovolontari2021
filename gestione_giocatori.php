<?php
require_once("Auth/Auth.php");
require_once("Database/GestioneDatiGenerici.php");
require_once("Templates/SezioniPagina.php");
require_once("Templates/Etichette.php");

$auth = PHPAuth\Auth::defaultAuth();
$dati = new GestioneDatiGenerici();

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: ./accesso_volontari.php');
	die();
}
?>
<html>
<head>
	<title>Gioco dei volontari - Gestione giocatori</title>
	<?php SezioniPagina::inserisciCssJavascript(); ?>
</head>
<body class="pb-5">
	<?php
		if (isset($messaggio) && isset($tipo_messaggio)) {
			Etichette::inserisciMessaggio($messaggio, $tipo_messaggio);
		}
	?>
	<div class="container">
	<?php
		$lista_giocatori = $dati->getListaGiocatori();

		if (sizeof($lista_giocatori) > 0) {
	?>
		<div class="text-center pt-4 pb-3">
			<h1>Gestione giocatori</h1>
		</div>

		<div>
			<div class="d-inline-block">
				<div id="ge-search" class="form-group position-relative text-center mr-auto pt-2">
					<label for="ge-searchbox">
						<i class="fa fa-search"></i>
					</label>
					<input id="ge-searchbox" type="text" name="game-search" placeholder="Cerca volontario" spellcheck="false">
				</div>
			</div>
			<div class="d-inline-block float-right pr-1">
				<a id="gv-search-aligned-button" class="btn btn-primary btn-sm gv-md-btn-width" href="./inserisci_iscrizione.php" role="button">Aggiungi</a>
			</div>
		</div>
		<div class="table-responsive">
			<table class="table">
				  <thead>
				    <tr class="d-flex">
				      <th scope="col" class="col-4">Nome</th>
				      <th scope="col" class="col-3">Badge</th>
				      <th scope="col" class="col-sm-4 col-xs-3 text-center">Sessioni</th>
					  <th scope="col" class="col-sm-1 col-xs-2"></th>
				    </tr>
				  </thead>
				<tbody>
	<?php
		foreach ($lista_giocatori as $giocatore) {
			$lista_sessioni = $dati->getListaSessioni(date("Y"));
			$sessioni_giocatore = $dati->getIscrizioniGiocatore($giocatore['id']);
	?>
					<tr class="gv-giocatore d-flex">
				      <td class="gv-nome col-4 text-break"><?php echo $giocatore['nome'] . ' ' . $giocatore['cognome']; ?></td>
				      <td class="gv-soprannome col-3 text-break"><?php echo $giocatore['soprannome']; ?></td>
				      <td class="col-sm-4 col-3 text-center">
	<?php
		foreach ($lista_sessioni as $sessione) {
			$data = new DateTime($sessione['data_sessione']);
	?>
						<div class="d-block d-md-inline-block px-sm-2">
							<label class="<?php if (in_array($sessione['id_sessione'], $sessioni_giocatore)) { echo 'text-success'; } else { echo 'text-danger'; } ?>">
								<?php echo $data->format('d/m'); ?>
							</label>
						</div>
	<?php
		}
	?>
					  </td>
					  <td class="col-sm-1 col-2">
					  	<a class="btn btn-sm btn-info" href="./inserisci_iscrizione.php?id=<?php echo $giocatore['id']; ?>"><i class="fa fa-pencil"></i></a>
					  </th>
				    </tr>
	<?php
		}
	?>
				</tbody>
			</table>
		</div>
	<?php
		} else {
	?>
		<div class="text-center pt-4 pb-2">
			<h1>Nessun utente da visualizzare</h1>
			<p class="lead">Per registrare nuovi utenti vai <a href="./inserisci_giocatore.php">qui</a>.</p>
		</div>
	<?php
		}
	?>
	</div>
	<?php SezioniPagina::inserisciFooter($auth); ?>
</body>
<script>
	$("#ge-searchbox").on("keyup", function(key) {
		var value = $(this).val().toLowerCase();

		if (key.which == 13) {
			$("tr.gv-giocatore").each(function(index) {
				$nome_volontario = $(this).find("td.gv-nome").text().toLowerCase();
				$soprannome_volontario = $(this).find("td.gv-soprannome").text().toLowerCase();
	
				if ($nome_volontario.indexOf(value) < 0 && $soprannome_volontario.indexOf(value) < 0) {
					$(this).addClass("d-none");
					$(this).removeClass("d-flex");
				}
				else {
					$(this).addClass("d-flex");
					$(this).removeClass("d-none");
				}
			});
		}
		else if (!value) {
			$("tr.gv-giocatore").each(function(index) {
				$(this).addClass("d-flex");
				$(this).removeClass("d-none");
			});
		}
	});
</script>
</html>