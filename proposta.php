<?php
require_once("Auth/Auth.php");
require_once("Database/GestioneDatiGenerici.php");
require_once("Database/GestioneDatiGioco.php");
require_once("Common/Funzioni.php");
require_once("Templates/Etichette.php");
require_once("Templates/SezioniPagina.php");

$auth = PHPAuth\Auth::defaultAuth();

if(!$auth->isLogged()) {
	header('Location: ./accesso_volontari.php');
	die();
} else {
    $dati_generici = new GestioneDatiGenerici();
    $dati_gioco = new GestioneDatiGioco();
    $funzioni = new Funzioni();

    $id_giocatore = $auth->getCurrentUser()['id'];
    $sessione_odierna = $dati_generici->getSessioneOdierna();

    $punti = 0;
    $punti_in_sospeso = 0;

    $id_sessione = 0;
    $sessione_valida = true;

    if (!$sessione_odierna) {
        $sessione_valida = false;
    } else {
        $id_sessione = $sessione_odierna['id_sessione'];
        $punti = $dati_gioco->getPuntiGiocatore($id_giocatore, $id_sessione);

        $sessione_valida = ($punti !== null) && (time() >= strtotime($sessione_odierna['ora_inizio'])) && (time() < strtotime($sessione_odierna['ora_fine']));
    }

    if (!$sessione_valida) {
        header('Location: ./punteggio.php');
        die();
    } else {
        $id_proposta = 0;
        $errore_volontari_indecisi = false;

        $proposta_da_giocare = $dati_gioco->getPropostaDaGiocare($id_giocatore, $id_sessione);

        if ($proposta_da_giocare != null) {
            header('Location: ./ruota.php');
            die();
        } else if ($_SERVER['REQUEST_METHOD'] === "POST") {
            if(isset($_POST['gv-id-proposta'])) {
                $id_proposta = $_POST['gv-id-proposta'];
                if ($id_proposta == 0 && isset($_POST['gv-id-volontario']) && isset($_POST['gv-punti-scommessi']) && isset($_POST['btn-invia'])) {
                    if ($_POST['gv-punti-scommessi'] < 0 || $_POST['gv-punti-scommessi'] > $punti || $_POST['gv-id-volontario'] == $id_giocatore) {
                        $messaggio = 'Stai cercando di fare qualcosa che non dovresti. Non provarci più.';
                        $tipo_messaggio = TipiMessaggio::Errore;
                    } else {
                        $proposta_altro_giocatore = $dati_gioco->getPropostaPendente($_POST['gv-id-volontario'], $id_sessione);
                        if ($proposta_altro_giocatore != null && $proposta_altro_giocatore['fk_ricevente'] == $id_giocatore && $proposta_altro_giocatore['punti_scommessi'] == $_POST['gv-punti-scommessi']) {
                            $dati_gioco->accettaProposta($proposta_altro_giocatore['id_proposta']);

                            $proposta_accettata = $dati_gioco->getPropostaDaGiocare($id_giocatore, $id_sessione);
                            if ($proposta_accettata == null) {
                                $messaggio = 'Si è verificato un errore in fase di salvataggio. Riprova per favore.';
                                $tipo_messaggio = TipiMessaggio::Errore;
                            } else {
                                header('Location: ./ruota.php');
                                die();
                            }
                        } else {
                            $dati_gioco->inserisciProposta($id_giocatore, $_POST['gv-id-volontario'], $id_sessione, $_POST['gv-punti-scommessi']);

                            $proposta_inserita = $dati_gioco->getPropostaPendente($id_giocatore, $id_sessione);
                            if ($proposta_inserita == null) {
                                $messaggio = 'Si è verificato un errore in fase di salvataggio. Riprova per favore.';
                                $tipo_messaggio = TipiMessaggio::Errore;
                            }
                        }
                    }
                } else if ($id_proposta > 0 && isset($_POST['btn-annulla'])) {
                    $dati_gioco->annullaProposta($id_giocatore, $id_proposta, $id_sessione);
                }
            }
        }

        $attesa_tentativi = false;
        $fascia_oraria = $funzioni->getFasciaOraria();
        if ($dati_gioco->countTentativiFasciaOraria($id_giocatore, $id_sessione, $fascia_oraria) > 0) {
            $attesa_tentativi = true;
        } else {
            $nome_volontario_scelto = '';
            $id_volontario_scelto = 0;
            $punti_scommessi = 1;
            
            $proposta_pendente = $dati_gioco->getPropostaPendente($id_giocatore, $id_sessione);
            if ($proposta_pendente != null) {
                $dati_volontario_scelto = $auth->getUser($proposta_pendente['fk_ricevente']);
    
                $id_proposta = $proposta_pendente['id_proposta'];
                $nome_volontario_scelto = $funzioni->costruisciNomeGiocatore($dati_volontario_scelto);
                $id_volontario_scelto = $proposta_pendente['fk_ricevente'];
                $punti_scommessi = $proposta_pendente['punti_scommessi'];
    
                $proposta_altro_giocatore = $dati_gioco->getPropostaPendente($proposta_pendente['fk_ricevente'], $id_sessione);
                if ($proposta_altro_giocatore != null) {
                    if ($proposta_altro_giocatore['fk_ricevente'] == $id_giocatore && $proposta_altro_giocatore['punti_scommessi'] != $punti_scommessi) {
                        $errore_volontari_indecisi = true;
                    }
                }
            } else {
                $id_proposta = 0;
                $punti_in_sospeso = $dati_gioco->getPuntiInSospeso($id_giocatore, $id_sessione);
            }
        }
    }
}
?>
<html>
<head>
	<title>Gioco dei volontari - Scegli</title>
	<?php SezioniPagina::inserisciCssJavascript(); ?>
</head>
<body class="pb-5">
	<?php
		if (isset($messaggio) && isset($tipo_messaggio)) {
			Etichette::inserisciMessaggio($messaggio, $tipo_messaggio);
		}
	?>
	<div class="container pb-4">
		<?php
            if ($attesa_tentativi) {
                if ($fascia_oraria->ora_fine != $sessione_odierna['ora_fine']) {
                    $titolo = "Devi aspettare!";
                    $sottotitolo = "Potrai girare di nuovo la ruota a partire dalle " . date("H:i", strtotime($fascia_oraria->ora_fine));
                } else {
                    $titolo = "Hai finito i tentativi";
                    $sottotitolo = "Grazie per aver giocato!";
                }
            } else if ($punti <= 0) {
                $titolo = "Non hai punti per giocare";
                $sottotitolo = "Ne vuoi altri? Chiedi in segreteria!";
            } else if (($punti - $punti_in_sospeso) <= 0) {
                $titolo = "Non hai punti per giocare";
                $sottotitolo = "Sono già tutti impegnati in altre giocate.<br/>Chiedi a chi hai scelto di girare la ruota!";
            } else {
                if (!$proposta_pendente) {
                    $titolo = "Scegli un volontario";
                    $sottotitolo = "Chi girerà la ruota per te?";
                } else if ($errore_volontari_indecisi) {
                    $titolo = "Mettetevi d'accordo!";
                } else {
                    $titolo = "Attendi";
                    $sottotitolo = "L'altro volontario deve inserire la sua scelta<br/>Dopo che l'avrà fatto ricarica la pagina";
                }
            }
		?>
		<div class="text-center pt-4">
			<h1><?php echo $titolo ?></h1>
			<?php
				if(isset($sottotitolo)) {
			?>
			<p class="lead">
				<?php echo $sottotitolo ?><br/>
			</p>
			<?php
				}
			?>
		</div>
        <?php if ($punti - $punti_in_sospeso > 0 && !$attesa_tentativi) { ?>
        <form class="p-3 mx-auto gv-max-width" method="post">
            <input id="gv-id-proposta" name="gv-id-proposta" type="hidden" value="<?php echo $id_proposta ?>">
            <div class="form-group row text-center">
                <div id="gv-messaggio-indecisi" class="mx-auto <?php if (!$errore_volontari_indecisi) { echo 'd-none'; } ?>">
                <?php
                    if ($errore_volontari_indecisi) {
                        $messaggio_indecisi = 'Il volontario scelto vuole giocare un numero diverso di punti (' . $proposta_altro_giocatore['punti_scommessi'] . ').<br/><br/>';
                        $messaggio_indecisi .= 'Dovete giocare gli stessi punti.<br/>';
                        $messaggio_indecisi .= 'Annullate una scelta e reinserite i dati.';
                        Etichette::inserisciMessaggio($messaggio_indecisi, TipiMessaggio::Errore);
                    }
                ?>
                </div>
                <label for="gv-volontario" class="col-sm-5 col-form-label">Volontario portafortuna</label>
                <div class="col-sm-7 pb-1">
                    <input id="gv-volontario" name="gv-volontario" class="form-control" placeholder="Cerca per nome o soprannome" required <?php if($id_volontario_scelto > 0) { echo 'readonly'; } ?>>
                </div>
                <input id="gv-id-volontario" name="gv-id-volontario" type="hidden" <?php if($id_volontario_scelto > 0) { echo 'value="' . $id_volontario_scelto . '"'; } ?>>
                <div id="gv-messaggio-sfortuna" class="mx-auto d-none">
                <?php
                    $messaggio_sfortuna = "Hai già scelto questo volontario in passato.<br/>Sceglierlo di nuovo ti porterà meno fortuna.";
                    Etichette::inserisciMessaggio($messaggio_sfortuna, TipiMessaggio::Avviso);
                ?>
                </div>
            </div>
            <div class="form-group row text-center">
				<label for="gv-punti-scommessi" class="col-sm-5 col-form-label">Punti da giocare</label>
                <div class="col-sm-7">
				    <input id="gv-punti-scommessi" name="gv-punti-scommessi" class="gv-input-width-sm no-focus" required readonly>
                    <small class="form-text text-muted">Dovete giocare entrambi gli stessi punti</small>
                </div>
			</div>
            <?php
                if (!$proposta_pendente) {
            ?>
            <div class="text-center pt-3">
				<button type="submit" name="btn-invia" class="btn btn-primary gv-btn-width">Invia scelta</button>
			</div>
            <?php } else { ?>
                <div class="text-center pt-3">
                    <button type="submit" name="btn-annulla" class="btn btn-danger gv-btn-width">Annulla scelta</button>
                    <div class="pt-2"><a class="btn btn-primary gv-btn-width" href="./proposta.php" role="button">Ricarica pagina</a></div>
                </div>
            <?php } ?>
        </form>
        <?php 
            }

            if ($attesa_tentativi && $punti - $punti_in_sospeso <= $dati_generici->getListaParametri()['soglia_punti_aggiuntivi']) {
        ?>
            <div class="text-center py-4">
                <p class>Nel frattempo vorresti ottenere qualche punto in più?<br/>Chiedi in segreteria!</p>
            </div>
        <?php
            }
        ?>
        <div class="text-center">
			<div><a class="btn btn-secondary gv-btn-width" href="./punteggio.php" role="button">Torna al punteggio</a></div>
		</div>
	</div>
	<?php SezioniPagina::inserisciFooter($auth); ?>
</body>
<script type="text/javascript">
    $( function() {
        <?php if (($punti - $punti_in_sospeso) > 0 && !$attesa_tentativi) { ?>
        // Spinner
        var spinner = $("#gv-punti-scommessi").spinner({
            min: 1,
            max: <?php echo ($punti - $punti_in_sospeso); ?>,
            start: 1
        });

        spinner.spinner("value", <?php echo $punti_scommessi ?>);
        <?php
            if ($proposta_pendente) { echo 'spinner.spinner("disable");'; }
        ?>

        // Autocomplete
        var autocomplete = $("#gv-volontario").autocomplete({
            source: "Ajax/RicercaVolontari.php",
            minLength: 3,
            select: function(event, ui) {
                $("#gv-volontario").val(ui.item.volontario);
                $("#gv-id-volontario").val(ui.item.id);

                $.ajax({ url: 'Ajax/ControllaProposte.php',
                    data: {
                        fk_volontario: ui.item.id
                    },
                    type: 'post',
                    success: function(response) {
                        ajax_result = $.parseJSON(response);
                        if (ajax_result.count > 0) {
                            $('#gv-messaggio-sfortuna').removeClass('d-none');
                        } else {
                            $('#gv-messaggio-sfortuna').addClass('d-none');
                        }
                    }
                });
        
                return false;
            }
        });

        autocomplete.autocomplete("widget").addClass("list-group");

        autocomplete.autocomplete("instance")._renderItem = function(ul, item) {
            return $("<li class=\"list-group-item\"></li>").data("item.autocomplete", item).append(item.volontario).appendTo(ul);
        };
        
        autocomplete.val(<?php echo '"' .$nome_volontario_scelto . '"' ?>);
        <?php } ?>
    });
</script>
</html>