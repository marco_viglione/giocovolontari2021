<?php
require_once("Auth/Auth.php");

$auth = PHPAuth\Auth::defaultAuth();

if($auth->isLogged())
{
	$auth->logout($auth->getCurrentSessionHash());
}

header('Location: ./accesso_volontari.php');
die();
?>