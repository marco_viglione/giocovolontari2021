<?php
require_once("Auth/Auth.php");
require_once("Database/GestioneDatiGenerici.php");
require_once("Database/GestioneDatiGioco.php");
require_once("Templates/SezioniPagina.php");
require_once("Templates/Etichette.php");

$auth = PHPAuth\Auth::defaultAuth();
$dati_generici = new GestioneDatiGenerici();
$dati_gioco = new GestioneDatiGioco();

$tipo_classifica = 0;

if(!$auth->isLogged() || !$auth->isAdmin()) {
	header('Location: ./accesso_volontari.php');
	die();
} else if ($_SERVER['REQUEST_METHOD'] === "POST")
{
	$lista_id_sessioni = array();
	foreach ($_POST as $key => $value) {
		if (substr($key, 0, 12) === 'gv-sessione-') {
			array_push($lista_id_sessioni, substr($key, 12, strlen($key)));
		} else if ($key == 'gv-radio-classifica') {
			$tipo_classifica = $value;
		}
	}

	if (sizeof($lista_id_sessioni) == 0) {
		$messaggio = "Devi selezionare almeno una sessione";
		$tipo_messaggio = TipiMessaggio::Errore;
		$lista_id_sessioni = null;
	}
}
?>
<html>
<head>
	<title>Gioco dei volontari - Classifica</title>
	<?php SezioniPagina::inserisciCssJavascript(); ?>
</head>
<body class="pb-5">
	<?php
		if (isset($messaggio) && isset($tipo_messaggio)) {
			Etichette::inserisciMessaggio($messaggio, $tipo_messaggio);
		}
	?>
	<div class="container">
	<?php
		$lista_sessioni = $dati_generici->getListaSessioni(date("Y"));

		if (!isset($lista_id_sessioni)) {
			$lista_id_sessioni = array_map(function($sessione){ return $sessione['id_sessione']; }, $lista_sessioni);
		}

		if (sizeof($lista_id_sessioni) > 0) {
			if ($tipo_classifica == TipoClassifica::Fortuna) {
				$classifica = $dati_gioco->generaClassificaFortuna($lista_id_sessioni);
			} else {
				$classifica = $dati_gioco->generaClassificaPunti($lista_id_sessioni);
			}
	?>
		<div class="text-center pt-4 pb-3">
			<h1>Classifica</h1>
		</div>
		<div id="accordion" class="pb-4">
			<div class="card">
				<div class="card-header text-center" id="gv-parametri-classifica-header" data-toggle="collapse" data-target="#gv-parametri-classifica-collapse" aria-expanded="true" aria-controls="gv-parametri-classifica-collapse">
					<a class="card-title">Parametri</a>
				</div>
				<div id="gv-parametri-classifica-collapse" class="collapse show text-center pt-3 mx-auto" aria-labelledby="gv-parametri-classifica-header" data-parent="#accordion">
					<form id="gv-parametri-classifica" method="post">
						<div>
							<p>Sessioni di gioco da considerare</p>
							<div class="form-group">
								<?php
								foreach ($lista_sessioni as $sessione) {
									$id_riga_sessione = "gv-sessione-" . $sessione['id_sessione'];
									$data = new DateTime($sessione['data_sessione']);
									$str_data = $data->format('d/m/Y');
									$riga_selezionata = in_array($sessione['id_sessione'], $lista_id_sessioni) ? "checked" : "";
									?>
									<div class="d-sm-inline-block px-2">
										<input name="<?php echo $id_riga_sessione ?>" type="checkbox" id="<?php echo $id_riga_sessione ?>" <?php echo $riga_selezionata ?>>
										<label class="checkbox-inline" for="<?php echo $id_riga_sessione ?>"><?php echo $str_data ?></label>
									</div>
									<?php
								}
								?>
							</div>
							<p>Tipologia classifica</p>
							<div class="form-group">
								<div class="d-sm-inline-block px-2">
									<input name="gv-radio-classifica" type="radio" id="gv-radio-c-0" value="<?php echo TipoClassifica::Punti; ?>" <?php if ($tipo_classifica == TipoClassifica::Punti) { echo 'checked'; } ?>>
									<label class="checkbox-inline" for="gv-radio-c-1">Punti</label>
								</div>
								<div class="d-sm-inline-block px-2">
									<input name="gv-radio-classifica" type="radio" id="gv-radio-c-2" value="<?php echo TipoClassifica::Fortuna; ?>" <?php if ($tipo_classifica == TipoClassifica::Fortuna) { echo 'checked'; } ?>>
									<label class="checkbox-inline" for="gv-radio-c-2">Fortuna</label>
								</div>
							</div>
						</div>
						<div>
							<button type="submit" name="btn-aggiorna" class="btn btn-primary">Aggiorna</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="table-responsive">
			<table class="table">
				  <thead>
				    <tr class="d-flex">
				      <th scope="col" class="col-1">#</th>
				      <th scope="col" class="col-4">Nome</th>
				      <th scope="col" class="col-3">Badge</th>
				      <th scope="col" class="col-4 text-center">Punteggio</th>
				    </tr>
				  </thead>
				<tbody>
	<?php
			$posizione = 0;
			foreach ($classifica as $posizione_giocatore) {
				$nome_giocatore = $posizione_giocatore['nome'] . ' ' . $posizione_giocatore['cognome'];
				$posizione += 1;
	?>
					<tr class="d-flex">
				      <th scope="row" class="col-1"><?php echo $posizione ?></th>
				      <td class="col-4 text-break"><?php echo $nome_giocatore ?></td>
				      <td class="col-3"><?php echo $posizione_giocatore['soprannome'] ?></td>
				      <td class="col-4 text-center">
						<?php
					  		echo $posizione_giocatore['punteggio'];
							if ($tipo_classifica == TipoClassifica::Fortuna) {
								echo " (" . $posizione_giocatore['tentativi'] . " giri)";
							}
						?>
					  </td>
				    </tr>
	<?php
			}
	?>
				</tbody>
			</table>
		</div>
	<?php
		} else {
	?>
		<div class="text-center pt-4 pb-2">
			<h1>Nessuna classifica da visualizzare</h1>
			<p class="lead">Non esistono sessioni di gioco disponibili.</p>
		</div>
	<?php
		}
	?>
	</div>
	<?php SezioniPagina::inserisciFooter($auth); ?>
</body>
</html>
<script>
	$(window).bind('resize load', function() {
		if ($(this).width() < 767) {
			$('.collapse').removeClass('show');
			$('.card-header').addClass('collapsed');
		} else {
			$('.collapse').addClass('show');
			$('.card-header').removeClass('collapsed');
		}
	});
</script>