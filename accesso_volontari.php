<?php
require_once("Auth/Auth.php");
require_once("Templates/Etichette.php");
require_once("Templates/SezioniPagina.php");

$auth = PHPAuth\Auth::defaultAuth();

$redirect = false;

if($auth->isLogged()) {
	$redirect = true;
}
else if ($_SERVER['REQUEST_METHOD'] === "POST")
{
	$username = $_POST['mail'];
	$password =  $_POST['password'];

	$loginResult = $auth->login($username, $password, true);

	if($loginResult['error']  == false)
	{
		$_COOKIE[$auth->config->cookie_name] = $loginResult['hash'];
		$redirect = true;
	}
	else
	{
		$messaggio = $loginResult['message'];
		$tipo_messaggio = TipiMessaggio::Errore;
	}
}

if ($redirect) {
	if ($auth->isAdmin()) {
		header('Location: ./admin.php');
	} else {
		header('Location: ./punteggio.php');
	}
	die();
}
?>
<html>
<head>
	<title>Gioco dei volontari - Accedi</title>
	<?php SezioniPagina::inserisciCssJavascript(); ?>
</head>
<body>
	<?php
		if (isset($messaggio) && isset($tipo_messaggio)) {
			Etichette::inserisciMessaggio($messaggio, $tipo_messaggio);
		}
	?>
	<div class="container">
		<div class="text-center pt-4">
			<h1>Ciao, volontario!</h1>
			<p class="lead">
				Accedi al gioco
			</p>
		</div>
		<form id="gv-login-form" class="gv-max-width p-3 mx-auto" method="post">
			<div class="form-group">
				<input name="mail" class="form-control" type="email" id="mail" placeholder="Indirizzo email" required>
			</div>
			<div class="form-group">
				<input type="password" name="password" class="form-control" id="password" placeholder="Password" required >
			</div>
			<div class="text-center pt-3">
				<button id="btn-login" type="submit" class="btn btn-primary">Accedi</button>
			</div>
		</form>
	</div>
</body>
</html>