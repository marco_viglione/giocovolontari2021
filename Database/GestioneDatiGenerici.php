<?php
require_once(dirname(__DIR__) . "/Common/Log.php");
require_once("Database.php");

class GestioneDatiGenerici {
    protected $pdo;

    private $log;

    /**
     * Costruttore
     */
    public function __construct() {
        $this->pdo = Database::getPDOConnection();
        $this->log = new Log();
    }

    /**
     * Recupera la lista delle sessioni di gioco dell'anno specificato
     * 
     * Parametri di input
     * @param int $anno (l'anno per cui si cercano le sessioni)
     * @param bool|false $solo_valide (opzionale: indica se recuperare solo le sessioni a partire dalla data odierna)
     * 
     * Risultato
     * @return array (PDO::FETCH_ASSOC)
     * 
     * Campi dei record di output:  
     * 'id_sessione'   => int    (identificativo della sessione)  
     * 'data_sessione' => string (data della sessione nel formato 'yyyy-MM-dd')  
     */
    public function getListaSessioni($anno, $solo_valide = false)
    {
        $sql = "SELECT id_sessione, data_sessione
                FROM sessioni_gioco
                WHERE YEAR(sessioni_gioco.data_sessione) = :anno";

        if ($solo_valide)
            $sql .= " AND sessioni_gioco.data_sessione >= CURDATE()";

        if ($query_statement = $this->pdo->prepare($sql)) {
            $query_statement->bindParam(':anno', $anno, PDO::PARAM_INT);
            $query_statement->execute();
            return $query_statement->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
            return null;
        }
    }

    /**
     * Recupera i dati della sessione di gioco corrispondente all'identificativo specificato.
     * 
     * Parametri di input
     * @param int $id_sessione (l'identificativo della sessione)
     *
     * Risultato
     * @return array (PDO::FETCH_ASSOC)
     * 
     * Campi dei record di output:  
     * 'data_sessione' => string (data della sessione nel formato 'yyyy-MM-dd')  
     * 'ora_inizio'    => string (ora di inizio della sessione nel formato 'hh:mm:ss')  
     * 'ora_fine'      => string (ora di fine della sessione nel formato 'hh:mm:ss')  
     */
    public function getDatiSessione($id_sessione)
    {
        $sql = "SELECT sessioni_gioco.data_sessione, sessioni_gioco.ora_inizio, sessioni_gioco.ora_fine
                FROM sessioni_gioco
                WHERE sessioni_gioco.id_sessione = :id_sessione";

        if ($query_statement = $this->pdo->prepare($sql)) {
            $query_statement->bindParam(':id_sessione', $id_sessione, PDO::PARAM_INT);
            $query_statement->execute();
            return $query_statement->fetch(PDO::FETCH_ASSOC);
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
            return null;
        }
    }

    /**
     * Recupera i dati della sessione di gioco odierna.
     *
     * Risultato
     * @return array (PDO::FETCH_ASSOC)
     * 
     * Campi dei record di output:  
     * 'id_sessione'   => int    (identificativo della sessione)  
     * 'data_sessione' => string (data della sessione nel formato 'yyyy-MM-dd')  
     * 'ora_inizio'    => string (ora di inizio della sessione nel formato 'hh:mm:ss')  
     * 'ora_fine'      => string (ora di fine della sessione nel formato 'hh:mm:ss')  
     */
    public function getSessioneOdierna()
    {
        $sql = "SELECT sessioni_gioco.id_sessione, sessioni_gioco.data_sessione, sessioni_gioco.ora_inizio, sessioni_gioco.ora_fine
                FROM sessioni_gioco
                WHERE sessioni_gioco.data_sessione = CURDATE()";

        if ($query_statement = $this->pdo->prepare($sql)) {
            $query_statement->execute();
            return $query_statement->fetch(PDO::FETCH_ASSOC);
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
            return null;
        }
    }

    /**
     * Associa un volontario alle sessioni a cui si è iscritto per giocare.
     *
     * Parametri di input
     * @param int $id_volontario (il volontario che si vuole iscrivere)
     * @param int $id_sessione (la sessione a cui si vuole iscrivere il volontario)
     * 
     * Risultato
     * @return bool (indica se l'operazione è andata a buon fine)
     */
    public function inserisciIscrizioneGiocatore($id_volontario, $id_sessione)
    {
        $sql = "INSERT IGNORE INTO giocatori_sessioni(fk_volontario, fk_sessione)
                VALUES (:id_volontario, :id_sessione)";

        if ($query_statement = $this->pdo->prepare($sql)) {
            $query_statement->bindParam(':id_volontario', $id_volontario, PDO::PARAM_INT);
            $query_statement->bindParam(':id_sessione', $id_sessione, PDO::PARAM_INT);
            return $query_statement->execute();
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
            return false;
        }
    }

    /**
    * Restituisce la lista degli identificativi delle sessioni a cui il volontario è iscritto.
    *
    * Parametri di input
    * @param int $id_volontario (identificativo del volontario che si vuole iscrivere)
    *
    * Risultato
    * @return array (PDO::FETCH_COLUMN)
    */
    public function getIscrizioniGiocatore($id_volontario) {
        $sql = "SELECT fk_sessione FROM giocatori_sessioni WHERE fk_volontario = :id_volontario";

        if ($query_statement = $this->pdo->prepare($sql)) {
            $query_statement->bindParam(':id_volontario', $id_volontario, PDO::PARAM_INT);
            $query_statement->execute();
            return $query_statement->fetchAll(PDO::FETCH_COLUMN);
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
            return null;
        }
    }

    /**
     * Elimina l'iscrizione a una sessione di un volontario.
     *
     * Parametri di input
     * @param int $id_volontario (il volontario di cui si vuole eliminare l'iscrizione)
     * @param int $id_sessione (la sessione per cui si vuole eliminare l'iscrizione)
     * 
     * Risultato
     * @return bool (indica se l'operazione è andata a buon fine)
     */
    public function eliminaIscrizioneGiocatore($id_volontario, $id_sessione) {
        $sql = "DELETE FROM giocatori_sessioni
                WHERE fk_volontario = :id_volontario AND fk_sessione = :id_sessione";

        if ($query_statement = $this->pdo->prepare($sql)) {
            $query_statement->bindParam(':id_volontario', $id_volontario, PDO::PARAM_INT);
            $query_statement->bindParam(':id_sessione', $id_sessione, PDO::PARAM_INT);
            return $query_statement->execute();
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
            return false;
        }
    }

    /**
     * Recupera la lista dei volontari iscritti alla sessione di gioco specificata.
     *
     * Parametri di input
     * @param int $id_sessione (La sessione di gioco)
     *
     * Risultato
     * @return array (PDO::FETCH_ASSOC)
     * 
     * Campi dei record di output:  
     * 'id'         => int    (identificativo del volontario)  
     * 'nome'       => string (nome del volontario)  
     * 'cognome'    => string (cognome del volontario)  
     * 'soprannome' => string (soprannome del volontario)
     */
    public function getGiocatoriSessione($id_sessione)
    {
        $sql = "SELECT DISTINCT phpauth_users.id, phpauth_users.nome, phpauth_users.cognome, phpauth_users.soprannome
                FROM phpauth_users
                INNER JOIN giocatori_sessioni ON giocatori_sessioni.fk_volontario = phpauth_users.id
                INNER JOIN sessioni_gioco ON giocatori_sessioni.fk_sessione = sessioni_gioco.id_sessione
                WHERE giocatori_sessioni.fk_sessione = :id_sessione
                ORDER BY phpauth_users.nome, phpauth_users.cognome";

        if ($query_statement = $this->pdo->prepare($sql)) {
            $query_statement->bindParam(':id_sessione', $id_sessione, PDO::PARAM_INT);
            $query_statement->execute();
            return $query_statement->fetch(PDO::FETCH_ASSOC);
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
            return null;
        }
    }

    /**
     * Cerca i giocatori iscritti alla sessione specificata basandosi su nome, cognome e soprannome
     *
     * Parametri di input
     * @param int $id_sessione (la sessione di gioco)
     * @param string $nome (il nome o soprannome, anche parziale, del giocatore)
     * @param array $id_giocatori_da_escludere (lista di identificativi dei giocatori da escludere dai risultati)
     *
     * Risultato
     * @return array (PDO::FETCH_ASSOC)
     * 
     * Campi dei record di output:  
     * 'id'         => int    (identificativo del volontario)  
     * 'volontario' => string (nome completo del volontario, con tra parentesi il soprannome se presente)
     */
    public function ricercaGiocatoriSessione($id_sessione, $nome, $id_giocatori_da_escludere)
    {
        $i = 0;
        $lista_id_esclusi = '';
        foreach ($id_giocatori_da_escludere as $id_escluso) {
            if ($i == 0)
                $lista_id_esclusi = ':val_'.$i;
            else
                $lista_id_esclusi .= ', :val_'.$i;
 
            $i = $i + 1;
        }

        $sql = "SELECT DISTINCT risultati.id, risultati.volontario FROM (
                    SELECT phpauth_users.id,
                    CASE
                        WHEN phpauth_users.soprannome IS NOT NULL AND trim(phpauth_users.soprannome) != ''
                        THEN CONCAT(phpauth_users.nome, ' ', phpauth_users.cognome, ' (', phpauth_users.soprannome, ')')
                        ELSE CONCAT(phpauth_users.nome, ' ', phpauth_users.cognome)
                        END as volontario
                    FROM phpauth_users
                    INNER JOIN giocatori_sessioni ON giocatori_sessioni.fk_volontario = phpauth_users.id
                    INNER JOIN sessioni_gioco ON giocatori_sessioni.fk_sessione = sessioni_gioco.id_sessione
                    WHERE giocatori_sessioni.fk_sessione = :id_sessione
                    ORDER BY phpauth_users.nome, phpauth_users.cognome) risultati
                WHERE risultati.volontario LIKE :nome
                AND risultati.id NOT IN (" . $lista_id_esclusi . ")";

        $filtro_nome = '%' . $nome . '%';

        if ($query_statement = $this->pdo->prepare($sql)) {
            $query_statement->bindParam(':id_sessione', $id_sessione, PDO::PARAM_INT);
            $query_statement->bindParam(':nome', $filtro_nome, PDO::PARAM_STR);

            $i = 0;
            foreach ($id_giocatori_da_escludere as $id_escluso) {
              $query_statement->bindValue(":val_".$i, $id_escluso, PDO::PARAM_INT);
              $i = $i + 1;
            }

            $query_statement->execute();
            return $query_statement->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
            return null;
        }
    }

    /**
     * Recupera la lista dei giocatori attivi.
     *
     * Risultato
     * @return array (campi dei record della lista: 'id', 'nome', 'cognome', 'soprannome', 'credenziali_inviate')
     * 
     * Campi dei record di output:  
     * 'id'                  => int    (identificativo del volontario)  
     * 'nome'                => string (nome del volontario)  
     * 'cognome'             => string (cognome del volontario)  
     * 'soprannome'          => string (soprannome del volontario)  
     * 'credenziali_inviate' => bool   (indica se le credenziali sono state inviate al giocatore)
     */
    public function getListaGiocatori(){
        $sql = "SELECT id, nome, cognome, soprannome, credenziali_inviate FROM phpauth_users WHERE role = 1 AND isactive = 1 ORDER BY nome, cognome";

        if ($query_statement = $this->pdo->prepare($sql)) {
            $query_statement->execute();
            return $query_statement->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
            return null;
        }
    }

    /**
     * Imposta il flag di credenziali inviate per un giocatore.
     * 
     * Parametri di input
     * @param int $id_utente (identificativo del giocatore)
     * 
     * Risultato
     * @return bool (indica se l'operazione è andata a buon fine)
     */
    public function impostaCredenzialiInviate($id_utente) {
        $sql = "UPDATE phpauth_users SET phpauth_users.credenziali_inviate = 1 WHERE phpauth_users.id = :id_utente";
  
        if ($query_statement = $this->pdo->prepare($sql)) {
            $query_statement->bindParam(':id_utente', $id_utente, PDO::PARAM_INT);
            return $query_statement->execute();
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
            return false;
        }
    }

    /**
     * Recupera la lista dei parametri di gioco.
     *
     * Risultato
     * @return array (PDO::FETCH_KEY_PAIR)
     * 
     * 'nome_parametro' => string (nome del parametro)  
     * 'valore'         => string (valore del parametro)
     */
    public function getListaParametri(){
      $sql = "SELECT nome_parametro, valore FROM parametri";

      if ($query_statement = $this->pdo->prepare($sql)) {
          $query_statement->execute();
          return $query_statement->fetchAll(PDO::FETCH_KEY_PAIR);
      } else {
          $error = $this->pdo->errno . ' ' . $this->pdo->error;
          $this->log->loggaErrore($error);
          return null;
      }
    }
}
?>