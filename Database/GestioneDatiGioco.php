<?php
require_once(dirname(__DIR__) . "/Common/Enum.php");
require_once(dirname(__DIR__) . "/Common/Funzioni.php");
require_once(dirname(__DIR__) . "/Common/Log.php");
require_once("Database.php");

class GestioneDatiGioco {
    protected $pdo;

    private $funzioni;
    private $log;

    /**
    * Costruttore
    */
    public function __construct() {
        $this->pdo = Database::getPDOConnection();
        $this->funzioni = new Funzioni();
        $this->log = new Log();
    }

    /**
    * Recupera il punteggio di un giocatore per la sessione specificata.
    *
    * Parametri di input
    * @param int $id_volontario: l'identificativo del volontario di cui si vogliono recuperare i punti
    * @param int $id_sessione: l'identificativo della sessione per cui si vogliono recuperare i punti
    *
    * Risultato
    * @return int
    */
    public function getPuntiGiocatore($id_volontario, $id_sessione)
    {
        $sql = "SELECT punti, punti_bonus FROM punti WHERE fk_volontario = :id_volontario AND fk_sessione = :id_sessione";

        if ($query_statement = $this->pdo->prepare($sql)) {
            $query_statement->bindParam(':id_volontario', $id_volontario, PDO::PARAM_INT);
            $query_statement->bindParam(':id_sessione', $id_sessione, PDO::PARAM_INT);
            $query_statement->execute();
            $result = $query_statement->fetch(PDO::FETCH_ASSOC);
            if ($result != null) {
                return $result['punti'] + $result['punti_bonus'];
            } else {
                return null;
            }
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
            return null;
        }
    }

    /**
     * Recupera il numero di punti scommessi in attesa di esito per il volontario e la sessione specificata
     * 
     * Parametri di input
     * @param int $id_volontario: l'identificativo del volontario di cui si vogliono recuperare i punti
     * @param int $id_sessione: l'identificativo della sessione per cui si vogliono recuperare i punti
     * 
     * Risultato
     * @return int
     */
    public function getPuntiInSospeso($id_volontario, $id_sessione) {
        $stato_proposta = StatoProposta::Accettata;
        $stato_esito_assente = StatoEsito::Assente;
        $stato_esito_calcolato = StatoEsito::Calcolato;

        $punti_in_sospeso = 0;

        $sql = "SELECT proposte.punti_scommessi
                FROM proposte
                LEFT JOIN esiti AS esito_proponente ON esito_proponente.fk_proposta = proposte.id_proposta
                AND esito_proponente.fk_volontario = proposte.fk_proponente
                LEFT JOIN esiti AS esito_ricevente ON esito_ricevente.fk_proposta = proposte.id_proposta
                AND esito_ricevente.fk_volontario = proposte.fk_ricevente
                WHERE proposte.stato = :stato_proposta
                AND proposte.fk_sessione = :id_sessione
                AND ((proposte.fk_proponente = :id_volontario AND esito_proponente.stato IN (:stato_esito_assente, :stato_esito_calcolato))
                OR (proposte.fk_ricevente = :id_volontario AND esito_ricevente.stato IN (:stato_esito_assente, :stato_esito_calcolato)))";

        if ($query_statement = $this->pdo->prepare($sql)) {
            $query_statement->bindParam(':id_volontario', $id_volontario, PDO::PARAM_INT);
            $query_statement->bindParam(':id_sessione', $id_sessione, PDO::PARAM_INT);
            $query_statement->bindParam(':stato_proposta', $stato_proposta, PDO::PARAM_INT);
            $query_statement->bindParam(':stato_esito_assente', $stato_esito_assente, PDO::PARAM_INT);
            $query_statement->bindParam(':stato_esito_calcolato', $stato_esito_calcolato, PDO::PARAM_INT);
            $query_statement->execute();
            foreach($query_statement as $proposta) {
                $punti_in_sospeso += $proposta['punti_scommessi'];
            }
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
        }

        return $punti_in_sospeso;
    }

    /**
     * Inserisce una nuova proposta di gioco
     * 
     * Parametri di input
     * @param int $id_proponente: l'identificativo del volontario che ha creato la proposta
     * @param int $id_ricevente: l'identificativo del volontario a cui è diretta la proposta
     * @param int $id_sessione: l'identificativo della sessione per cui si vuole creare la proposta
     * @param int $punti_scommessi: i punti da giocare
     */
    public function inserisciProposta($id_proponente, $id_ricevente, $id_sessione, $punti_scommessi) {
        $stato_proposta = StatoProposta::Pendente;
        $altre_proposte_pendenti = $this->getPropostaPendente($id_proponente, $id_sessione);

        $sql = "INSERT IGNORE INTO proposte(fk_proponente, fk_ricevente, fk_sessione, punti_scommessi, stato, timestamp)
                VALUES (:id_proponente, :id_ricevente, :id_sessione, :punti_scommessi, :stato, NOW())";

        if ($altre_proposte_pendenti == null && $query_statement = $this->pdo->prepare($sql)) {
            $query_statement->bindParam(':id_proponente', $id_proponente, PDO::PARAM_INT);
            $query_statement->bindParam(':id_ricevente', $id_ricevente, PDO::PARAM_INT);
            $query_statement->bindParam(':id_sessione', $id_sessione, PDO::PARAM_INT);
            $query_statement->bindParam(':punti_scommessi', $punti_scommessi, PDO::PARAM_INT);
            $query_statement->bindParam(':stato', $stato_proposta, PDO::PARAM_INT);
            $query_statement->execute();
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
        }
    }

    /**
     * Modifica lo stato della proposta specificata
     * 
     * Parametri di input
     * @param int $id_proposta: l'identificativo della proposta da modificare
     * @param int $stato: l'identificativo dello stato da impostare (vedi Enum:StatoProposta)
     */
    public function setStatoProposta($id_proposta, $stato) {
        $sql = "UPDATE proposte SET stato = :stato WHERE id_proposta = :id_proposta";

        if ($query_statement = $this->pdo->prepare($sql)) {
            $query_statement->bindParam(':id_proposta', $id_proposta, PDO::PARAM_INT);
            $query_statement->bindParam(':stato', $stato, PDO::PARAM_INT);
            $query_statement->execute();
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
        }
    }

    /**
     * Recupera i dati della proposta specificata
     * 
     * Parametri di input
     * @param int $id_proposta: l'identificativo della proposta da recuperare
     * 
     * Risultato
     * @return record
     * 
     * Campi attesi del record di output:
     * id_proposta: l'identificativo della proposta
     * fk_proponente: l'identificativo del volontario che ha creato la proposta
     * fk_ricevente: l'identificativo del volontario scelto per giocare
     * punti_scommessi: il numero di punti giocati
     * stato: l'identificativo dello stato (vedi Enum:StatoProposta)
     */
    public function getProposta($id_proposta) {
        $sql = "SELECT id_proposta, fk_proponente, fk_ricevente, punti_scommessi, stato
                FROM proposte WHERE id_proposta = :id_proposta";

        if ($query_statement = $this->pdo->prepare($sql)) {
            $query_statement->bindParam(':id_proposta', $id_proposta, PDO::PARAM_INT);
            $query_statement->execute();
            return $query_statement->fetch(PDO::FETCH_ASSOC);
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
        }
    }

    /**
     * Recupera i dati della proposta di gioco pendente per il volontario e la sessione specificata
     * 
     * Parametri di input
     * @param int $id_volontario: l'identificativo del volontario di cui si vuole recuperare la proposta
     * @param int $id_sessione: l'identificativo della sessione per cui si vuole recuperare la proposta
     *
     * Risultato
     * @return record
     * 
     * Campi attesi del record di output:
     * id_proposta: l'identificativo della proposta
     * fk_ricevente: l'identificativo del volontario scelto per giocare
     * punti_scommessi: il numero di punti giocati
     */
    public function getPropostaPendente($id_volontario, $id_sessione) {
        $stato_proposta = StatoProposta::Pendente;

        $sql = "SELECT id_proposta, fk_ricevente, punti_scommessi
                FROM proposte
                WHERE fk_proponente = :id_volontario
                AND fk_sessione = :id_sessione
                AND stato = :stato_proposta";

        if ($query_statement = $this->pdo->prepare($sql)) {
            $query_statement->bindParam(':id_volontario', $id_volontario, PDO::PARAM_INT);
            $query_statement->bindParam(':id_sessione', $id_sessione, PDO::PARAM_INT);
            $query_statement->bindParam(':stato_proposta', $stato_proposta, PDO::PARAM_INT);
            $query_statement->execute();
            return $query_statement->fetch(PDO::FETCH_ASSOC);
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
        }
    }

    /**
     * Recupera i dati di una proposta accettata in attesa di risultato da parte del volontario specificato
     * 
     * Parametri di input
     * @param int $id_volontario: l'identificativo del volontario di cui si vuole recuperare la proposta
     * @param int $id_sessione: l'identificativo della sessione per cui si vuole recuperare la proposta
     * 
     * Risultato
     * @return record
     * 
     * Campi attesi del record di output:
     * id_proposta: l'identificativo della proposta
     * fk_volontario: l'identificativo dell'altro volontario associato alla proposta
     * punti_scommessi: il numero di punti giocati
     * esito: l'esito della giocata (se già calcolato ma non ancora assegnato)
     * stato_esito: l'identificativo dello stato dell'esito (vedi Enum:StatoEsito)
     */
    public function getPropostaDaGiocare($id_volontario, $id_sessione) {
        $stato_proposta = StatoProposta::Accettata;
        $stato_esito_assente = StatoEsito::Assente;
        $stato_esito_calcolato = StatoEsito::Calcolato;
        
        $sql = "SELECT proposte.id_proposta, proposte.punti_scommessi,
                CASE WHEN proposte.fk_proponente = :id_volontario THEN esito_ricevente.fk_volontario ELSE esito_proponente.fk_volontario END AS fk_volontario,
                CASE WHEN proposte.fk_proponente = :id_volontario THEN esito_ricevente.esito ELSE esito_proponente.esito END AS esito,
                CASE WHEN proposte.fk_proponente = :id_volontario THEN esito_ricevente.stato ELSE esito_proponente.stato END AS stato_esito
                FROM proposte
                LEFT JOIN esiti AS esito_proponente ON esito_proponente.fk_proposta = proposte.id_proposta
                AND esito_proponente.fk_volontario = proposte.fk_proponente
                LEFT JOIN esiti AS esito_ricevente ON esito_ricevente.fk_proposta = proposte.id_proposta
                AND esito_ricevente.fk_volontario = proposte.fk_ricevente
                WHERE proposte.stato = :stato_proposta
                AND proposte.fk_sessione = :id_sessione
                AND ((proposte.fk_proponente = :id_volontario AND esito_ricevente.stato IN (:stato_esito_assente, :stato_esito_calcolato))
                OR (proposte.fk_ricevente = :id_volontario AND esito_proponente.stato IN (:stato_esito_assente, :stato_esito_calcolato)))";

        if ($query_statement = $this->pdo->prepare($sql)) {
            $query_statement->bindParam(':id_volontario', $id_volontario, PDO::PARAM_INT);
            $query_statement->bindParam(':id_sessione', $id_sessione, PDO::PARAM_INT);
            $query_statement->bindParam(':stato_proposta', $stato_proposta, PDO::PARAM_INT);
            $query_statement->bindParam(':stato_esito_assente', $stato_esito_assente, PDO::PARAM_INT);
            $query_statement->bindParam(':stato_esito_calcolato', $stato_esito_calcolato, PDO::PARAM_INT);
            $query_statement->execute();
            return $query_statement->fetch(PDO::FETCH_ASSOC);
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
        }
    }

    /**
     * Restituisce il numero di proposte accettate associate alla coppia di volontari specificata
     * 
     * Parametri di input
     * @param int $id_volontario_1: l'identificativo di uno qualunque dei volontari associati alla proposta
     * @param int $id_volontario_2: l'identificativo dell'altro volontario associato alla proposta
     * 
     * Risultato
     * @return int
     */
    public function countProposteVolontari($id_volontario_1, $id_volontario_2) {
        $stato_proposta = StatoProposta::Accettata;

        $sql = "SELECT id_proposta
                FROM proposte
                WHERE stato = :stato_proposta
                AND ((fk_proponente = :id_volontario_1 AND fk_ricevente = :id_volontario_2)
                OR (fk_proponente = :id_volontario_2 AND fk_ricevente = :id_volontario_1))";

        if ($query_statement = $this->pdo->prepare($sql)) {
            $query_statement->bindParam(':id_volontario_1', $id_volontario_1, PDO::PARAM_INT);
            $query_statement->bindParam(':id_volontario_2', $id_volontario_2, PDO::PARAM_INT);
            $query_statement->bindParam(':stato_proposta', $stato_proposta, PDO::PARAM_INT);
            $query_statement->execute();
            return $query_statement->rowCount();
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
        }
    }

    /**
     * Accetta una proposta e crea i relativi record di esito
     * 
     * Parametri di input
     * @param int $id_proposta: l'identificativo della proposta che si vuole accettare
     */
    public function accettaProposta($id_proposta) {
        $dati_proposta = $this->getProposta($id_proposta);

        if ($dati_proposta != null) {
            $stato_proposta = StatoProposta::Accettata;
            $stato_esito = StatoEsito::Assente;

            $success_cambio_stato = null;
            $success_creazione_esito_proponente = null;
            $success_creazione_esito_ricevente = null;

            if($this->pdo->beginTransaction()) {
                $sql_proposta = "UPDATE proposte SET stato = :stato_proposta WHERE id_proposta = :id_proposta";

                if ($query_statement = $this->pdo->prepare($sql_proposta)) {
                    $query_statement->bindParam(':id_proposta', $id_proposta, PDO::PARAM_INT);
                    $query_statement->bindParam(':stato_proposta', $stato_proposta, PDO::PARAM_INT);
                    $success_cambio_stato = $query_statement->execute();
                } else {
                    $error = $this->pdo->errno . ' ' . $this->pdo->error;
                    $this->log->loggaErrore($error);
                }

                $sql_esito = "INSERT IGNORE INTO esiti(fk_proposta, fk_volontario, esito, stato, timestamp)
                                  VALUES(:id_proposta, :id_volontario, NULL, :stato_esito, NOW())";

                if ($query_statement = $this->pdo->prepare($sql_esito)) {
                    $query_statement->bindParam(':id_proposta', $id_proposta, PDO::PARAM_INT);
                    $query_statement->bindParam(':id_volontario', $dati_proposta['fk_proponente'], PDO::PARAM_INT);
                    $query_statement->bindParam(':stato_esito', $stato_esito, PDO::PARAM_INT);
                    $success_creazione_esito_proponente = $query_statement->execute();
                } else {
                    $error = $this->pdo->errno . ' ' . $this->pdo->error;
                    $this->log->loggaErrore($error);
                }
                if ($query_statement = $this->pdo->prepare($sql_esito)) {
                    $query_statement->bindParam(':id_proposta', $id_proposta, PDO::PARAM_INT);
                    $query_statement->bindParam(':id_volontario', $dati_proposta['fk_ricevente'], PDO::PARAM_INT);
                    $query_statement->bindParam(':stato_esito', $stato_esito, PDO::PARAM_INT);
                    $success_creazione_esito_ricevente = $query_statement->execute();
                } else {
                    $error = $this->pdo->errno . ' ' . $this->pdo->error;
                    $this->log->loggaErrore($error);
                }

                if ($success_cambio_stato && $success_creazione_esito_proponente && $success_creazione_esito_ricevente) {
                    $this->pdo->commit();
                } else {
                    $this->pdo->rollBack();
                }
            }
        }
    }

    /**
     * Annulla una proposta di gioco
     * 
     * Parametri di input
     * @param int $id_volontario: l'identificativo del volontario che ha creato la proposta di gioco
     * @param int $id_proposta: l'identificativo della proposta che si vuole annullare
     * @param int $id_sessione: l'identificativo della sessione per cui si vuole annullare la proposta
     */
    public function annullaProposta($id_volontario, $id_proposta, $id_sessione) {
        $stato_proposta = StatoProposta::Pendente;

        $sql = "DELETE FROM proposte
                WHERE id_proposta = :id_proposta
                AND fk_proponente = :id_volontario
                AND fk_sessione = :id_sessione
                AND stato = :stato_proposta";
        
        if ($query_statement = $this->pdo->prepare($sql)) {
            $query_statement->bindParam(':id_proposta', $id_proposta, PDO::PARAM_INT);
            $query_statement->bindParam(':id_volontario', $id_volontario, PDO::PARAM_INT);
            $query_statement->bindParam(':id_sessione', $id_sessione, PDO::PARAM_INT);
            $query_statement->bindParam(':stato_proposta', $stato_proposta, PDO::PARAM_INT);
            $query_statement->execute();
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
        }
    }

    /**
     * Imposta l'esito per la proposta e il volontario specificato, aggiornando anche lo stato dell'esito
     * 
     * Parametri di input
     * @param int $id_proposta: l'identificativo della proposta per cui si vuole impostare l'esito
     * @param int $id_volontario: l'identificativo del volontario per cui si vuole impostare l'esito
     * @param int $esito: l'esito da impostare
     */
    public function setEsitoGioco($id_proposta, $id_volontario, $esito) {
        $stato_esito = StatoEsito::Calcolato;
        
        $sql = "UPDATE esiti
                SET esito = :esito, stato = :stato_esito
                WHERE fk_proposta = :id_proposta
                AND fk_volontario = :id_volontario";

        if ($query_statement = $this->pdo->prepare($sql)) {
            $query_statement->bindParam(':id_proposta', $id_proposta, PDO::PARAM_INT);
            $query_statement->bindParam(':id_volontario', $id_volontario, PDO::PARAM_INT);
            $query_statement->bindParam(':esito', $esito, PDO::PARAM_INT);
            $query_statement->bindParam(':stato_esito', $stato_esito, PDO::PARAM_INT);
            $query_statement->execute();
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
        }
    }

    /**
     * Assegna punti a un giocatore per la sessione specificata.
     * 
     * Parametri di input
     * @param int $id_volontario: l'identificativo del volontario a cui si vogliono assegnare punti
     * @param int $id_sessione: l'identificativo della sessione per cui si vogliono assegnare punti
     * @param int $punti: i punti da assegnare
     * @param bool $bonus: determina se impostare i punti come bonus
     * 
     * Risultato
     * @return bool
     */
    public function assegnaPuntiGiocatore($id_volontario, $id_sessione, $punti, $bonus = false)
    {
        if ($this->getPuntiGiocatore($id_volontario, $id_sessione) === null) {
            $sql = "INSERT IGNORE INTO punti(fk_volontario, fk_sessione, punti, punti_bonus)
                    VALUES (:id_volontario, :id_sessione, :punti, 0)";
        } else {
            if ($bonus) {
                $sql = "UPDATE punti SET punti_bonus = punti_bonus + :punti
                        WHERE fk_volontario = :id_volontario AND fk_sessione = :id_sessione";
            } else {
                $sql = "UPDATE punti SET punti = punti + :punti
                        WHERE fk_volontario = :id_volontario AND fk_sessione = :id_sessione";
            }
        }

        if ($query_statement = $this->pdo->prepare($sql)) {
            $query_statement->bindParam(':id_volontario', $id_volontario, PDO::PARAM_INT);
            $query_statement->bindParam(':id_sessione', $id_sessione, PDO::PARAM_INT);
            $query_statement->bindParam(':punti', $punti, PDO::PARAM_INT);
            return $query_statement->execute();
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
            return false;
        }
    }

    /**
     * Assegna punti a un giocatore per la sessione specificata e aggiorna lo stato dell'esito in transazione
     * 
     * Parametri di input
     * @param int $id_volontario: l'identificativo del volontario a cui si vogliono assegnare punti
     * @param int $id_proposta: l'identificativo della proposta per cui si vogliono assegnare punti
     * 
     * Risultato
     * @return boolean
     */
    public function assegnaRisultatoGioco($id_volontario, $id_proposta) {
        $risultato = false;
        $punti = 0;
        
        $stato_esito_precedente = StatoEsito::Calcolato;
        $stato_esito_nuovo = StatoEsito::Assegnato;
        
        $sql_controllo_esito = "SELECT esiti.esito, proposte.punti_scommessi
                                FROM esiti INNER JOIN proposte ON esiti.fk_proposta = proposte.id_proposta
                                WHERE esiti.fk_volontario = :id_volontario AND esiti.fk_proposta = :id_proposta
                                AND esiti.stato = :stato_esito_precedente";

        if ($query_statement = $this->pdo->prepare($sql_controllo_esito)) {
            $query_statement->bindParam(':id_volontario', $id_volontario, PDO::PARAM_INT);
            $query_statement->bindParam(':id_proposta', $id_proposta, PDO::PARAM_INT);
            $query_statement->bindParam(':stato_esito_precedente', $stato_esito_precedente, PDO::PARAM_INT);
            $query_statement->execute();
            $dati_esito = $query_statement->fetch(PDO::FETCH_ASSOC);
            if ($dati_esito != null) {
                $punti = $this->funzioni->calcolaRisultato($dati_esito['punti_scommessi'], $dati_esito['esito']);
            }
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
        }

        if ($punti != 0) {
            if($this->pdo->beginTransaction()) {
                $success_punti = null;
                $success_esito = null;

                $sql_punti = "UPDATE punti SET punti = punti + :punti
                              WHERE fk_volontario = :id_volontario
                              AND fk_sessione IN (SELECT fk_sessione FROM proposte WHERE id_proposta = :id_proposta)";

                if ($query_statement = $this->pdo->prepare($sql_punti)) {
                    $query_statement->bindParam(':id_volontario', $id_volontario, PDO::PARAM_INT);
                    $query_statement->bindParam(':id_proposta', $id_proposta, PDO::PARAM_INT);
                    $query_statement->bindParam(':punti', $punti, PDO::PARAM_INT);
                    $success_punti = $query_statement->execute();
                } else {
                    $error = $this->pdo->errno . ' ' . $this->pdo->error;
                    $this->log->loggaErrore($error);
                }

                $sql_esito = "UPDATE esiti SET stato = :stato_esito_nuovo
                              WHERE fk_volontario = :id_volontario
                              AND fk_proposta = :id_proposta
                              AND stato = :stato_esito_precedente";

                if ($query_statement = $this->pdo->prepare($sql_esito)) {
                    $query_statement->bindParam(':id_volontario', $id_volontario, PDO::PARAM_INT);
                    $query_statement->bindParam(':id_proposta', $id_proposta, PDO::PARAM_INT);
                    $query_statement->bindParam(':stato_esito_precedente', $stato_esito_precedente, PDO::PARAM_INT);
                    $query_statement->bindParam(':stato_esito_nuovo', $stato_esito_nuovo, PDO::PARAM_INT);
                    $success_esito = $query_statement->execute();
                } else {
                    $error = $this->pdo->errno . ' ' . $this->pdo->error;
                    $this->log->loggaErrore($error);
                }

                if ($success_punti && $success_esito) {
                    $this->pdo->commit();
                    $risultato = true;
                } else {
                    $this->pdo->rollBack();
                }
            }
        }

        return $risultato;
    }

    /**
     * Verifica se per la sessione è presente un esito da visualizzare
     * 
     * Parametri di input
     * @param int $id_volontario: l'identificativo del volontario di cui si vogliono recuperare i punti
     * @param int $id_sessione: l'identificativo della sessione per cui si vogliono recuperare i punti
     * 
     * Risultato
     * @return record
     * 
     * Campi attesi del record
     * id_proposta: identificativo della proposta
     * punti_scommessi: il numero di punti scommessi
     * esito: l'identificativo dell'esito (vedi Enum:RisultatoGioco)
     * fk_giratore: identificativo del volontario che ha generato l'esito
     */
    public function getEsitiDaVisualizzare($id_volontario, $id_sessione) {
        $stato_proposta = StatoProposta::Accettata;
        $stato_esito = StatoEsito::Assegnato;
        
        $sql = "SELECT proposte.id_proposta, proposte.punti_scommessi, esiti.esito,
                CASE WHEN esiti.fk_volontario = proposte.fk_proponente
                     THEN proposte.fk_ricevente 
                     ELSE proposte.fk_proponente 
                     END AS fk_giratore
                FROM proposte
                INNER JOIN esiti ON esiti.fk_proposta = proposte.id_proposta
                AND (esiti.fk_volontario = proposte.fk_proponente OR esiti.fk_volontario = proposte.fk_ricevente)
                WHERE esiti.fk_volontario = :id_volontario
                AND proposte.fk_sessione = :id_sessione
                AND proposte.stato = :stato_proposta
                AND esiti.stato = :stato_esito";

        if ($query_statement = $this->pdo->prepare($sql)) {
            $query_statement->bindParam(':id_volontario', $id_volontario, PDO::PARAM_INT);
            $query_statement->bindParam(':id_sessione', $id_sessione, PDO::PARAM_INT);
            $query_statement->bindParam(':stato_proposta', $stato_proposta, PDO::PARAM_INT);
            $query_statement->bindParam(':stato_esito', $stato_esito, PDO::PARAM_INT);
            $query_statement->execute();
            return $query_statement;
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
            return null;
        }
    }

    /**
     * Aggiorna lo stato dell'esito specificato
     * 
     * Parametri di input
     * @param int $id_proposta: l'identificativo della proposta di cui si vuole aggiornare l'esito
     * @param int $id_volontario: l'identificativo del volontario di cui si vuole aggiornare l'esito
     * @param int $stato: l'identificativo dello stato da impostare (vedi Enum:StatoEsito)
     */
    public function aggiornaStatoEsito($id_proposta, $id_volontario, $stato) {
        $sql = "UPDATE esiti SET stato = :stato
                WHERE fk_proposta = :id_proposta AND fk_volontario = :id_volontario";

        if ($query_statement = $this->pdo->prepare($sql)) {
            $query_statement->bindParam(':id_proposta', $id_proposta, PDO::PARAM_INT);
            $query_statement->bindParam(':id_volontario', $id_volontario, PDO::PARAM_INT);
            $query_statement->bindParam(':stato', $stato, PDO::PARAM_INT);
            $query_statement->execute();
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
        }
    }

    /**
     * Restituisce il numero di tentativi fatto dal volontario specificato nella fascia oraria specificata
     * 
     * Parametri di input
     * @param int $id_volontario: l'identificativo del Volontario di cui si vogliono verificare i tentativi
     * @param int $id_sessione: l'identificativo della sessione di cui si vogliono verificare i tentativi
     * @param FasciaOraria $fascia_oraria: la fascia oraria in cui devono cadere i tentativi
     * 
     * Risultato
     * @return int
     */
    public function countTentativiFasciaOraria($id_volontario, $id_sessione, $fascia_oraria) {
        $stato_proposta = StatoProposta::Accettata;
        $stato_esito_assegnato = StatoEsito::Assegnato;
        $stato_esito_visualizzato = StatoEsito::Visualizzato;

        $count_proposte = 0;
        $count_esiti = 0;

        $inizio_fascia_oraria = date('Y-m-d H:i:s', strtotime($fascia_oraria->ora_inizio));
        $fine_fascia_oraria = date('Y-m-d H:i:s', strtotime($fascia_oraria->ora_fine));

        $sql_proposte = "SELECT id_proposta FROM proposte
                         WHERE fk_proponente = :id_volontario
                         AND fk_sessione = :id_sessione
                         AND stato = :stato_proposta
                         AND timestamp BETWEEN :inizio_fascia_oraria AND :fine_fascia_oraria";

        if ($query_statement = $this->pdo->prepare($sql_proposte)) {
            $query_statement->bindParam(':id_volontario', $id_volontario, PDO::PARAM_INT);
            $query_statement->bindParam(':id_sessione', $id_sessione, PDO::PARAM_INT);
            $query_statement->bindParam(':stato_proposta', $stato_proposta, PDO::PARAM_INT);
            $query_statement->bindParam(':inizio_fascia_oraria', $inizio_fascia_oraria, PDO::PARAM_STR);
            $query_statement->bindParam(':fine_fascia_oraria', $fine_fascia_oraria, PDO::PARAM_STR);
            $query_statement->execute();
            $count_proposte = $query_statement->rowCount();
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
        }

        $sql_esiti = "SELECT proposte.id_proposta FROM proposte
                      INNER JOIN esiti ON esiti.fk_proposta = proposte.id_proposta AND esiti.fk_volontario = proposte.fk_proponente
                      WHERE proposte.fk_ricevente = :id_volontario
                      AND proposte.fk_sessione = :id_sessione
                      AND proposte.stato = :stato_proposta
                      AND esiti.stato IN (:stato_esito_assegnato, :stato_esito_visualizzato)
                      AND esiti.timestamp BETWEEN :inizio_fascia_oraria AND :fine_fascia_oraria";

        if ($query_statement = $this->pdo->prepare($sql_esiti)) {
            $query_statement->bindParam(':id_volontario', $id_volontario, PDO::PARAM_INT);
            $query_statement->bindParam(':id_sessione', $id_sessione, PDO::PARAM_INT);
            $query_statement->bindParam(':stato_proposta', $stato_proposta, PDO::PARAM_INT);
            $query_statement->bindParam(':stato_esito_assegnato', $stato_esito_assegnato, PDO::PARAM_INT);
            $query_statement->bindParam(':stato_esito_visualizzato', $stato_esito_visualizzato, PDO::PARAM_INT);
            $query_statement->bindParam(':inizio_fascia_oraria', $inizio_fascia_oraria, PDO::PARAM_STR);
            $query_statement->bindParam(':fine_fascia_oraria', $fine_fascia_oraria, PDO::PARAM_STR);
            $query_statement->execute();
            $count_esiti = $query_statement->rowCount();
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
        }

        return $count_proposte + $count_esiti;
    }

    /**
     * Genera la classifica basata sui punti
     * 
     * Parametri di input
     * @param int $lista_sessioni: lista degli identificativi delle sessioni per cui generare la classifica
     * 
     * Risultato
     * @return record
     * 
     * Campi attesi del record
     * nome: nome del volontario
     * cognome: cognome del volontario
     * soprannome: soprannome del volontario
     * punteggio: il totale dei punti del volontario
     * punti_bonus: i punti bonus ricevuti dal volontario
     */
    public function generaClassificaPunti($lista_sessioni) {
        $stato_proposta = StatoProposta::Accettata;
        
        $i = 0;
        $lista_id_sessioni = '';
        foreach ($lista_sessioni as $id_sessione) {
            if ($i == 0)
                $lista_id_sessioni .= ':val_'.$i;
            else
                $lista_id_sessioni .= ', :val_'.$i;
 
            $i = $i + 1;
        }

        $inner_sql = "SELECT
                        phpauth_users.nome,
                        phpauth_users.cognome,
                        phpauth_users.soprannome,
                        SUM(punti.punti + punti.punti_bonus) AS punteggio,
                        SUM(punti.punti_bonus) AS punti_bonus
                      FROM
                        punti
                      INNER JOIN
                        phpauth_users ON phpauth_users.id = punti.fk_volontario
                      WHERE
                        punti.fk_sessione IN (" . $lista_id_sessioni . ")
                      AND EXISTS(SELECT * FROM proposte
                                 WHERE proposte.fk_sessione = punti.fk_sessione
                                 AND proposte.stato = :stato_proposta
                                 AND (proposte.fk_proponente = punti.fk_volontario OR proposte.fk_ricevente = punti.fk_volontario)
                                )
                      GROUP BY
                        phpauth_users.nome,
                        phpauth_users.cognome,
                        phpauth_users.soprannome";

        $sql = "SELECT * FROM (" . $inner_sql . ") classifica ORDER BY classifica.punteggio DESC, classifica.punti_bonus";

        if ($query_statement = $this->pdo->prepare($sql)) {
            $i = 0;
            foreach ($lista_sessioni as $id_sessione) {
              $query_statement->bindValue(":val_".$i, $id_sessione, PDO::PARAM_INT);
              $i = $i + 1;
            }

            $query_statement->bindParam(':stato_proposta', $stato_proposta, PDO::PARAM_INT);
  
            $query_statement->execute();
            return $query_statement;
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
            return null;
        }
    }

    /**
     * Genera la classifica basata sulla fortuna
     * 
     * Parametri di input
     * @param int $lista_sessioni: lista degli identificativi delle sessioni per cui generare la classifica
     * 
     * Risultato
     * @return record
     * 
     * Campi attesi del record
     * nome: nome del volontario
     * cognome: cognome del volontario
     * soprannome: soprannome del volontario
     * punteggio: il totale dei punti del volontario
     * tentativi: il numero di giocate fatte dal volontario
     */
    public function generaClassificaFortuna($lista_sessioni) {
        $stato_proposta = StatoProposta::Accettata;
        $stato_esito_assegnato = StatoEsito::Assegnato;
        $stato_esito_visualizzato = StatoEsito::Visualizzato;
        
        $i = 0;
        $lista_id_sessioni = '';
        foreach ($lista_sessioni as $id_sessione) {
            if ($i == 0)
                $lista_id_sessioni .= ':val_'.$i;
            else
                $lista_id_sessioni .= ', :val_'.$i;
 
            $i = $i + 1;
        }

        $sql_esiti = "SELECT
                        phpauth_users.nome,
                        phpauth_users.cognome,
                        phpauth_users.soprannome,
                        CASE WHEN proposte.fk_proponente = phpauth_users.id THEN esito_ricevente.esito ELSE esito_proponente.esito END AS esito,
                        CASE WHEN proposte.fk_proponente = phpauth_users.id THEN esito_ricevente.stato ELSE esito_proponente.stato END AS stato_esito
                      FROM
                        phpauth_users
                      INNER JOIN
                        proposte ON proposte.fk_proponente = phpauth_users.id OR proposte.fk_ricevente = phpauth_users.id
                      LEFT JOIN
                        esiti AS esito_proponente ON esito_proponente.fk_proposta = proposte.id_proposta AND esito_proponente.fk_volontario = proposte.fk_proponente
                      LEFT JOIN
                        esiti AS esito_ricevente ON esito_ricevente.fk_proposta = proposte.id_proposta AND esito_ricevente.fk_volontario = proposte.fk_ricevente
                      WHERE
                        proposte.stato = :stato_proposta
                      AND
                        proposte.fk_sessione IN (" . $lista_id_sessioni . ")";

        $sql_punti_esiti = "SELECT
                                nome,
                                cognome,
                                soprannome,
                                CASE WHEN esito = " . RisultatoGioco::Raddoppia . " THEN 3
                                     WHEN esito = " . RisultatoGioco::Vinci . " THEN 2
                                     WHEN esito = " . RisultatoGioco::VinciPoco . " THEN 1
                                     WHEN esito = " . RisultatoGioco::PerdiPoco . " THEN -1
                                     ELSE -2
                                END AS punti_esito
                            FROM
                                (" . $sql_esiti . ") esiti
                            WHERE
                                stato_esito IN (:esito_assegnato, :esito_visualizzato)";

        $inner_sql = "SELECT
                        nome,
                        cognome,
                        soprannome,
                        SUM(punti_esito) AS punteggio,
                        SUM(1) AS tentativi
                      FROM
                        (" . $sql_punti_esiti . ") punti_esiti
                      GROUP BY
                        nome,
                        cognome,
                        soprannome";

        $sql = "SELECT * FROM (" . $inner_sql . ") classifica ORDER BY classifica.punteggio DESC, classifica.tentativi DESC";

        if ($query_statement = $this->pdo->prepare($sql)) {
            $i = 0;
            foreach ($lista_sessioni as $id_sessione) {
              $query_statement->bindValue(":val_".$i, $id_sessione, PDO::PARAM_INT);
              $i = $i + 1;
            }

            $query_statement->bindParam(':stato_proposta', $stato_proposta, PDO::PARAM_INT);
            $query_statement->bindParam(':esito_assegnato', $stato_esito_assegnato, PDO::PARAM_INT);
            $query_statement->bindParam(':esito_visualizzato', $stato_esito_visualizzato, PDO::PARAM_INT);
  
            $query_statement->execute();
            return $query_statement;
        } else {
            $error = $this->pdo->errno . ' ' . $this->pdo->error;
            $this->log->loggaErrore($error);
            return null;
        }
    }

}
?>