<?php

class Database {
    
    /**
     * Metodo pubblico per accedere alla corretta connessione al DB.
     */
    public static function getPDOConnection()
    {
        return Database::__MAMPPDO();
    }
    
    /**
     * Metodo per accedere al database di sviluppo.
     */
    private static function __MAMPPDO()
    {
        $pdo = new PDO("mysql:host=localhost;dbname=gioco_volontari", "root", "root");
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;    
    }
    
    /**
     * Metodo per accedere al database di produzione.
     */
    private static function __ProdPDO()
    {
        $pdo = new PDO("mysql:host=localhost:3306;dbname=giocaost_gioco_volontari;charset=utf8", "gioca_admin", "");
        return $pdo;
    }
}
?>