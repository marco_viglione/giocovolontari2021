<?php
require_once("Auth/Auth.php");
require_once("Database/GestioneDatiGenerici.php");
require_once("Database/GestioneDatiGioco.php");
require_once("Common/Funzioni.php");
require_once("Templates/Etichette.php");
require_once("Templates/SezioniPagina.php");

$auth = PHPAuth\Auth::defaultAuth();

if(!$auth->isLogged()) {
	header('Location: ./accesso_volontari.php');
	die();
} else {
    $dati_generici = new GestioneDatiGenerici();
    $dati_gioco = new GestioneDatiGioco();
    $funzioni = new Funzioni();

    $id_giocatore = $auth->getCurrentUser()['id'];
    $sessione_odierna = $dati_generici->getSessioneOdierna();

    $id_sessione = 0;
    $sessione_valida = true;

    if (!$sessione_odierna) {
        $sessione_valida = false;
    } else {
        $id_sessione = $sessione_odierna['id_sessione'];
        $punti = $dati_gioco->getPuntiGiocatore($id_giocatore, $id_sessione);

        $sessione_valida = ($punti !== null) && (time() >= strtotime($sessione_odierna['ora_inizio'])) && (time() < strtotime($sessione_odierna['ora_fine']));
    }

    if (!$sessione_valida) {
        header('Location: ./punteggio.php');
        die();
    } else {
        $dati_proposta = $dati_gioco->getPropostaDaGiocare($id_giocatore, $id_sessione);

        if ($dati_proposta == null) {
            header('Location: ./proposta.php');
            die();
        } else {
            $dati_volontario_scelto = $auth->getUser($dati_proposta['fk_volontario']);

            if ($dati_proposta['stato_esito'] == StatoEsito::Assente) {
                $num_proposte_volontari = $dati_gioco->countProposteVolontari($id_giocatore, $dati_proposta['fk_volontario']);
                $riduzione_peso = 0.5 + $num_proposte_volontari/2;

                $probabilita_pesate = array(
                    RisultatoGioco::Raddoppia => round(20/$riduzione_peso),
                    RisultatoGioco::Vinci => round(20/$riduzione_peso),
                    RisultatoGioco::VinciPoco => round(20/$riduzione_peso),
                    RisultatoGioco::PerdiPoco => 20,
                    RisultatoGioco::Perdi => 20
                );

                $esito = RisultatoGioco::Vinci;
                $rand = mt_rand(1, (int) array_sum($probabilita_pesate));

                foreach ($probabilita_pesate as $key => $value) {
                    $rand -= $value;
                    if ($rand <= 0) {
                        $esito = $key;
                        break;
                    }
                }

                $dati_gioco->setEsitoGioco($dati_proposta['id_proposta'], $dati_proposta['fk_volontario'], $esito);
                $dati_proposta['esito'] = $esito;
            }

            if (isset($_POST['error-field']) && $_POST['error-field'] == 1) {
                $messaggio = "Si è verificato un errore. Gira di nuovo la ruota.";
                $tipo_messaggio = TipiMessaggio::Errore;
            }
        }
    }
}
?>
<html>
<head>
	<title>Gioco dei volontari - Ruota</title>
	<?php SezioniPagina::inserisciCssJavascript(); ?>
    <link rel="stylesheet" href="css/wheel.css" >
</head>
<body class="pb-5">
	<?php
		if (isset($messaggio) && isset($tipo_messaggio)) {
			Etichette::inserisciMessaggio($messaggio, $tipo_messaggio);
		}
	?>
	<div class="container pb-4">
		<?php
			$titolo = "La Ruota della Vanvera";
            $sottotitolo = "Premi il centro della ruota!"
		?>
		<div class="text-center pt-4">
			<h1><?php echo $titolo ?></h1>
			<?php
				if(isset($sottotitolo)) {
			?>
			<p class="lead">
				<?php echo $sottotitolo ?><br/>
			</p>
			<?php
				}
			?>
		</div>
        <div class="text-center pt-4">
            <h5>Stai giocando per <?php
                                    $volontario_scelto = '';
                                    if ($dati_volontario_scelto['soprannome'] != null) {
                                        $volontario_scelto = $dati_volontario_scelto['soprannome'];
                                    } else {
                                        $volontario_scelto = $dati_volontario_scelto['nome'] . ' ' . $dati_volontario_scelto['cognome'];
                                    }
                                    echo $volontario_scelto;
                                  ?>
            </h5>
        </div>
        <div id="wheel-wrapper">     
            <div id="wheel">
                <div id="inner-wheel">
                    <div class="sec"><span class="fa <?php echo $funzioni->getDatiRisultato(RisultatoGioco::Vinci)->icona; ?>" style="left: -5px;"></span></div>
                    <div class="sec"><span class="fa <?php echo $funzioni->getDatiRisultato(RisultatoGioco::Perdi)->icona; ?>" style="left: -5px;"></span></div>
                    <div class="sec"><span class="fa <?php echo $funzioni->getDatiRisultato(RisultatoGioco::VinciPoco)->icona; ?>" style="left: -5px;"></span></div>
                    <div class="sec"><span class="fa <?php echo $funzioni->getDatiRisultato(RisultatoGioco::PerdiPoco)->icona; ?>" style="left: -5px;"></span></div>
                    <div class="sec"><span class="fa <?php echo $funzioni->getDatiRisultato(RisultatoGioco::Raddoppia)->icona; ?>" style="left: -5px;"></span></div>
                </div>       
                <div id="spin">
                    <div id="inner-spin" class="unspinned"></div>
                </div>
                <div id="shine"></div>
            </div>
        </div>
        <div class="text-center pt-5">
			<div><a class="btn btn-secondary gv-btn-width" href="./punteggio.php" role="button">Torna al punteggio</a></div>
		</div>
	</div>
    <form id="error-form" class="d-none" method="post">
        <input type="hidden" id="error-field" name="error-field" value="0" />
    </form>
	<?php SezioniPagina::inserisciFooter($auth); ?>
    <div class="modal fade" id="result-modal" tabindex="-1" role="dialog" aria-labelledby="result-modal-label" aria-hidden="true">
        <?php $dati_risultato = $funzioni->getDatiRisultato($dati_proposta['esito']); ?>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="result-modal-label"><?php echo $dati_risultato->titolo;?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <div class="fa fa-3 <?php echo $dati_risultato->icona ?>"></div>
                    <div class="pt-2"><?php echo $dati_risultato->testo; ?></div>
                    <div class="pt-2 font-weight-bold"><?php
                            $punti_ottenuti = $funzioni->calcolaRisultato($dati_proposta['punti_scommessi'], $dati_proposta['esito']);
                            $testo_esito = $volontario_scelto . ' ';
                            if ($punti_ottenuti > 0) {
                                $testo_esito .= 'ha vinto ' . $punti_ottenuti . ' punti';
                            } else {
                                $testo_esito .= 'ha perso ' . abs($punti_ottenuti) . ' punti';
                            }
                            echo $testo_esito;
                        ?>
                    </div>
                </div>
                <div class="modal-footer d-block">
                    <div class="text-center">
                        <a class="btn btn-secondary gv-btn-width" href="./punteggio.php" role="button">Torna al punteggio</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    // Imposto i gradi iniziali, ovvero 360 * il numero di giri di ruota che voglio fare
    var degree = 1800;

    $(document).ready(function(){
        
        var spinned = false;
        var update_success = false;

        $('#spin').click(function(){
            if (!spinned) {
                spinned = true;
                $('#inner-spin').removeClass('unspinned');

                var extraDegree = <?php echo $dati_proposta['esito']; ?> * 72; // Ogni 72 gradi c'è un cambio di settore
                var spinVariance = (Math.random() * 20) + 1; // Aggiungo una differenza casuale entro il settore risultante
                var spinVarianceSign = Math.round(Math.random()) ? 1 : -1; // Estraggo se la differenza deve essere positiva o negativa
                totalDegree = degree + extraDegree + (spinVariance * spinVarianceSign);

                setTimeout(function() {
                    if (update_success) {
                        $('#result-modal').modal('show');
                    } else {
                        $('#error-field').val(1);
                        $('#error-form').submit();
                    }
                }, 6000);
                
                /* Logica per il tilt del bottone centrale */
                $('#wheel .sec').each(function(){
                    var t = $(this);
                    var noY = 0;
                    
                    var c = 0;
                    var n = 700;	
                    var interval = setInterval(function() {
                        c++;				
                        if (c === n) { 
                            clearInterval(interval);				
                        }	
                            
                        var aoY = t.offset().top;
                        //console.log(aoY);
                        
                        /* Un offset minore o uguale a 179 indica che mi trovo al limite di un settore */
                        if(aoY < 179){
                            //console.log('<<<<<<<<');
                            $('#spin').addClass('spin');
                            setTimeout(function() { 
                                $('#spin').removeClass('spin');
                            }, 100);	
                        }
                    }, 10);
                    
                    $('#inner-wheel').css({
                        'transform' : 'rotate(' + totalDegree + 'deg)'			
                    });
                
                    noY = t.offset().top;
                });

                $.ajax({ url: 'Ajax/AssegnaRisultato.php',
                    data: {
                        fk_proposta: <?php echo $dati_proposta['id_proposta']; ?>,
                        fk_volontario: <?php echo $dati_proposta['fk_volontario']; ?>
                    },
                    type: 'post',
                    success: function(response) {
                        ajax_result = $.parseJSON(response);
                        update_success = ajax_result.success;
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        update_success = false;
                    }
                });
            }
        });
    });
</script>
</html>